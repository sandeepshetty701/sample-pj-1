var awsController = require('../controllers/awsController')

async function s3_upload(bucketParams) {
  let S3 = await awsController.getS3Instance();
  try {
    return await S3.upload(bucketParams).promise();
  } catch (err) {
    console.log(err);
    return null;
  }
}

async function s3_delete_folder(bucket, path) {
  try {
    const listParams = {
      Bucket: bucket,
      Prefix: path + '/'
    };
    let S3 = await awsController.getS3Instance();
    const listedObjects = await S3.listObjectsV2(listParams).promise();

    if (listedObjects.Contents.length === 0) return;

    let deleteParams = {
      Bucket: bucket,
      Delete: {
        Objects: []
      }
    };

    listedObjects.Contents.forEach(({
      Key
    }) => {
      deleteParams.Delete.Objects.push({
        Key
      });
    });

    let result = await S3.deleteObjects(deleteParams).promise();

    deleteParams = {
      Bucket: bucket,
      Key: path
    };
    result = await S3.deleteObject(deleteParams).promise();

    if (listedObjects.IsTruncated) await s3_delete_folder(bucket, path);
  } catch (err) {
    console.log(err);
    return null;
  }
}

async function __uploadSingleFile(bucket, path, file) {
  let S3 = await awsController.getS3Instance();
  var params = {
    Bucket: bucket,
    Key: path,
    Body: file.buffer,
    ACL: 'public-read'
  };
  try {
    await S3.upload(params).promise();
    return 'Uploaded successfully!';
  } catch (e) {
    return e.message;
  }
}

module.exports = {
  s3_upload: s3_upload,
  s3_delete_folder: s3_delete_folder,
  uploadSingleFile: __uploadSingleFile
}