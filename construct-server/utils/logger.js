'use strict'
const log4js = require("log4js");

require('dotenv').config();

log4js.configure({
    appenders: {
      out: { type: 'stdout' },
      app: { type: 'file', filename: 'logs/app-logs.log' }
    },
    categories: {
      default: { appenders: [ 'app' ], level: 'debug' }
    }
});
function getLogger(instance) {
    let logger = log4js.getLogger(instance);
    logger.level = process.env.LOG_LEVEL;
    return logger;
}

module.exports = {getLogger};