var mongodb = require('../config/mongodb');
var logger = require('../utils/logger').getLogger('Script');

require('dotenv').config();

var WorkFlow = mongodb.models.WorkFlow;

function createDefautWorkflowInDB(workFlowObj) {
    return new Promise(async (resolve, reject) => {
        var workflow = new WorkFlow(workFlowObj);
        workflow.save({
            new: true
        }, async function (err, workFlowData) {
            if (!err) {
                logger.info(workFlowData)
            } else {
                logger.info({
                    "name": "DB_Error",
                    "description": err,
                    "message": "Internal Server Error"
                });
            }
        });
    });
}

var wf = {
    "type" : "DRONE_DEFAULT",
    "steps" : [
        {
            "name" : "FLIGHT"
        },
        {
            "name" : "UPLOAD"
        },
        {
            "name" : "IMAGE_PROCESS",
            "child_steps" : [
                {
                    "name" : "ODM_PROCESS"
                },
                {
                    "name" : "PC_POST",
                    "child_steps" : [
                        {
                            "name" : "LASZIP"
                        },
                        {
                            "name" : "SNIP_PC"
                        },
                        {
                            "name" : "POTREE_CONV"
                        }
                    ]
                },
                {
                    "name" : "ORTHO_POST",
                    "child_steps" : [
                        {
                            "name" : "SNIP_ORTHO"
                        },
                        {
                            "name" : "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name" : "CAMERAS"
                },
                {
                    "name" : "THUMBNAILS"
                },
                {
                    "name" : "UPLOAD_S3"
                }
            ]
        }
    ]
 }

createDefautWorkflowInDB(wf);