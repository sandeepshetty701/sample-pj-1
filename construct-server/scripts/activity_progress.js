var storage = require('../lib/storage');
const mongoose = require('mongoose');

let feb16 = require('/Users/apple/Downloads/finalwbsprogress/wbs_p_feb16_final.json');
let feb28 = require('/Users/apple/Downloads/finalwbsprogress/wbs_p_feb28_final.json');
let mar11 = require('/Users/apple/Downloads/finalwbsprogress/wbs_p_mar11_final.json');
let mar28 = require('/Users/apple/Downloads/finalwbsprogress/wbs_p_mar28_final.json');
let apr17 = require('/Users/apple/Downloads/finalwbsprogress/wbs_p_apr17_final.json');
let may2 = require('/Users/apple/Downloads/finalwbsprogress/wbs_p_may2_final.json');
let may29 = require('/Users/apple/Downloads/finalwbsprogress/wbs_p_may29_final.json');
let jun10 = require('/Users/apple/Downloads/finalwbsprogress/wbs_p_jun10_final.json');

let dataset = [
    {
        date: '2019-02-16T04:00:00.000Z',
        value: feb16
    },
    {
        date: '2019-02-28T04:00:00.000Z',
        value: feb28
    },
    {
        date: '2019-03-11T04:00:00.000Z',
        value: mar11
    },
    {
        date: '2019-03-28T04:00:00.000Z',
        value: mar28
    },
    {
        date: '2019-04-17T04:00:00.000Z',
        value: apr17
    },
    {
        date: '2019-05-02T04:00:00.000Z',
        value: may2
    },
    {
        date: '2019-05-29T04:00:00.000Z',
        value: may29
    },
    {
        date: '2019-06-10T04:00:00.000Z',
        value: jun10
    }
]

// setIgnoreFlags()
// .then(res => {
//     console.log(res)
// })
// .catch(err => {
//     console.log(err)
// })

// resetSnapshot()
// .then(res => {
//     console.log(res)
// })
// .catch(err => {
//     console.log(err)
// })

updateProgress()
.then(res => {
    console.log(res)
})
.catch(err => {
    console.log(err)
})

async function updateProgress() {
    for (let j = 0; j < dataset.length; j++) {
        let data = dataset[j].value;
        let date = dataset[j].date;
        let temp = JSON.stringify(data);
        data = JSON.parse(temp.replace('5.2..', '5.2.'))
        console.log(date);
        for (let i = 0; i < data.length; i++) {
            await updateActivityProgress(data[i].wbs_id, new Date(date), data[i].planned, data[i].actual)
        }
    }
}

async function updateActivityProgress(wbs_id, date, planned, actual) {
    let query = {
        wbs_id: wbs_id,
        'snapshot.date': date
    };
    try {
        let updateBody = {};
        if (actual) {
            updateBody['snapshot.$.actual'] = actual;
        }
        if (planned) {
            updateBody['snapshot.$.planned'] = planned;
        }

        let body = {
            actual: actual,
            date: date
        }

        if (planned) {
            body.planned = planned;
        }

        let mUpdate = await storage.mongoDB.activities.update_activity(query, {
            '$set': updateBody
        }, true);
        if (!mUpdate) {
            delete query['snapshot.date'];
            mUpdate = await storage.mongoDB.activities.update_activity(query, {
                $push: {
                    snapshot: {
                        $each: [body],
                        $sort: {
                            date: 1
                        }
                    }
                }
            });
        }
        let updatedActivity = mUpdate;
        let parent_project = JSON.parse(JSON.stringify(mUpdate)).project;
        let parent_activity = mUpdate.parent_activity;

        await calculateProgress(parent_activity, date, parent_project);

        return updatedActivity;
    } catch (err) {
        console.log(err)
        throw throwErrorMessage(err);
    }
}

async function calculateProgress(parent_activity, date, parent_project) {
    let mUpdate;
    let progress;
    while (parent_activity) {
        progress = await storage.mongoDB.activities.getActivityProgressForDate(date, {
            parent_activity: parent_activity
        });

        mUpdate = await storage.mongoDB.activities.update_activity({
            _id: parent_activity
        }, {
                $pull: {
                    snapshot: {
                        date: date
                    }
                }
            }, true);
        mUpdate = await storage.mongoDB.activities.update_activity({
            _id: parent_activity
        }, {
                $push: {
                    snapshot: {
                        $each: [progress],
                        $sort: {
                            date: 1
                        }
                    }
                }
            }, true);

        parent_activity = mUpdate.parent_activity;
        console.log(parent_activity, progress)
    }

    progress = await storage.mongoDB.activities.getActivityProgressForDate(date, {
        parent_activity: null,
        project: new mongoose.Types.ObjectId(parent_project)
    });

    mUpdate = await storage.mongoDB.projects.updateProject({
        _id: parent_project
    }, {
            $pull: {
                snapshot: {
                    date: date
                }
            }
        });
    mUpdate = await storage.mongoDB.projects.updateProject({
        _id: parent_project
    }, {
            $push: {
                snapshot: {
                    $each: [progress],
                    $sort: {
                        date: 1
                    }
                }
            }
        });
    parent_project = JSON.parse(JSON.stringify(mUpdate)).project;

    while (parent_project) {
        progress = await storage.mongoDB.projects.getProjectProgressForDate(date, parent_project);

        mUpdate = await storage.mongoDB.projects.updateProject({
            _id: parent_project
        }, {
                $pull: {
                    snapshot: {
                        date: date
                    }
                }
            });
        mUpdate = await storage.mongoDB.projects.updateProject({
            _id: parent_project
        }, {
                $push: {
                    snapshot: {
                        $each: [progress],
                        $sort: {
                            date: 1
                        }
                    }
                }
            });

        parent_project = JSON.parse(JSON.stringify(mUpdate)).project;
    }
}

async function throwErrorMessage(err) {
    throw ({
        status: 'Error',
        statusCode: 500,
        description: err,
        message: 'Database Error'
    });
}

async function resetSnapshot() {
    let dates = ['2019-12-31T04:00:00.000Z', '2019-01-31T04:00:00.000Z']
    for (let i = 0; i < dates.length; i++) {
        await calculateProgress('5c6a95ffefc6860e16b7e0f6', dates[i], '5c5d410ca1d53e0f89c9ca83')
    }
}

async function setIgnoreFlags() {
    let wbsList = ['5.1.1', '5.1.2', '5.1.3', '5.1.9', '5.1.10', '5.1.12', '5.1.13',
        '5.1.14', '5.1.15', '5.1.16', '5.1.17', '5.1.18', '5.1.20', '5.2.1', , '5.2.8',
        '5.2.15', , '5.2.16', '5.2.17', , '5.2.18', '5.2.19', , '5.2.20', '5.2.21'];
    for (let i = 0; i < wbsList.length; i++) {
        let mUpdate = await storage.mongoDB.activities.update_activity({
            wbs_id: wbsList[i]
        }, {
                $set: {
                    'properties.ignore': true
                }
            });
    }
}