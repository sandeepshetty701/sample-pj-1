'use strict'

const mongodb = require('../config/mongodb');
var fs = require('fs');
const storage = require('../lib/storage').mongoDB;
const blogic = require('../lib/blogic');

var Project = mongodb.models.Project;
var Tileset = mongodb.models.Tileset;

let createdBy = '5e5f3ba87cfd04006b122f43';
let token = 's7mCyTKLjh2LnfzXnpUqr9yl9mfMYVoCploaeJO0iWX';
let decoded = {
    user: createdBy,
    token: token
}
let body = {
    "location" : [ 
        17.4277841445694, 
        78.4129235491037
    ],
    "projectType" : "Other",
    "name" : "Indis Project",
    "address" : "19/525, Sri Nagar Colony, Krishna Nagar, Hyderabad, Telangana 500033, India",
    "organisation" : "5e5f3ba9a603c6015d86a893",
    "scanDetails" : {
        "interior" : {
            "name" : "Interior",
            "area" : {
                "quantity" : 12345,
                "units" : "sq.ft"
            },
            "repeat" : {
                "count" : 1,
                "frequency" : "Daily"
            }
        }
    }
};

let blocks = ['TOWER - A', 'TOWER - B'];
// let floors = ['STILT FLOOR', 'FIRST FLOOR', 'SECOND FLOOR', 'THIRD FLOOR', 'FOURTH FLOOR', 'FIFTH FLOOR', 'ABOVE TERRACE WORKS']
let floors = ['Ground Floor', '1st Floor', '2nd Floor', '3rd Floor', '4th Floor', '5th Floor', '6th Floor', '7th Floor',
'8th Floor', '9th Floor', '10th Floor', '11th Floor', '12th Floor', '13th Floor', '14th Floor', '15th Floor', '16th Floor', 'Above Terrace']

// importDetails();

// exportDetails();

createProject().then(res => console.log(res)).catch(err => console.log(err))

// createClubHouse().then(res => console.log(res)).catch(err => console.log(err))

// createFlatsForBlock('B').then(res => console.log(res)).catch(err => console.log(err))

// createFloors('5d3189034f7dd7003ee64359').then(res => console.log(res)).catch(err => console.log(err))

async function createClubHouse() {
    let project = await blogic.projects.createProject(decoded, body)
    return project;
}

async function createFlatsForBlock(block) {
    let floorIds = ['5da83dd076ae900ef2237776', '5da83dd276ae900ef2237778', '5da83dd376ae900ef223777a',
        '5da83dd576ae900ef223777c', '5da83dd776ae900ef223777e']
    for(let i = 0; i < floorIds.length; i++) {
        console.log('--------------------')
        console.log(floorIds[i]);
        await createFlats(floorIds[i], block, i + 1);
    }
}

async function createProject() {
    let project = await blogic.projects.createProject(decoded, body);
    console.log(project.result._id)
    await createBlocks(project.result._id);
}

async function createBlocks(parent) {
    for(let i = 0; i < blocks.length; i++) {
        let mBody = JSON.parse(JSON.stringify(body));
        mBody.name = blocks[i];
        mBody.parent = parent;
        console.log(`---- CREATING ${blocks[i]} ----`);
        let block = await blogic.projects.createProject(decoded, mBody);
        await createFloors(blocks[i], block.result._id);
    }
}

async function createFloors(block, parent) {
    for(let i = 0; i < floors.length; i++) {
        let mBody = JSON.parse(JSON.stringify(body));
        mBody.name = floors[i];
        mBody.parent = parent;
        console.log(`---- CREATING ${floors[i]} ----`);
        let floor = await blogic.projects.createProject(decoded, mBody);
        await createFlats(floor.result._id, block.replace('TOWER - ', ''), i);
    }
}

async function createFlats(parent, block, floor) {
    for(let i = 1; i <= 14; i++) {
        let mBody = body;
        mBody.name = `${block}-${floor}${i > 9 ? '' + i : '0' + i}`;
        mBody.parent = parent;
        console.log(`---- CREATING ${mBody.name} ----`);
        let project = await blogic.projects.createProject(decoded, mBody);
        console.log(project.name);
    }
}

function importDetails() {
    // let data = fs.readFileSync('./scripts/files/project.json');
    // let project = JSON.parse(data);
    // Project.create(project)
    // .then(res => {
    //     console.log(res);
    // })
    // .catch(err => {
    //     console.log("============= ERROR ===============");
    //     console.log(err);
    // })

    let data = fs.readFileSync('./scripts/files/tilesets.json');
    let tilesets = JSON.parse(data);
    Tileset.insertMany(tilesets)
    .then(res => {
        console.log(res.length);
    })
    .catch(err => {
        console.log("============= ERROR ===============");
        console.log(err);
    })

    // data = fs.readFileSync('./scripts/files/activities.json');
    // let activities = JSON.parse(data);
    // Activity.insertMany(activities)
    // .then(res => {
    //     console.log(res.length);
    // })
    // .catch(err => {
    //     console.log("============= ERROR ===============");
    //     console.log(err);
    // })
}

function exportDetails() {
    exportProject(project)
        .then((res) => {
            fs.writeFile('./scripts/files/project.json', buildProject(JSON.parse(JSON.stringify(res))), 'utf8', (e) => {
                console.log('DONE Project');
            })
        })
        .catch((err) => {
            console.log(err);
        })


    exportTilesets(project)
        .then((res) => {
            let tilesets = [];
            for (let i = 0; i < res.length; i++) {
                tilesets.push(buildTileset(JSON.parse(JSON.stringify(res[i]))));
            }
            fs.writeFile('./scripts/files/tilesets.json', `${JSON.stringify(tilesets)}`, 'utf8', (e) => {
                console.log('DONE Tilesets');
            })
        })
        .catch((err) => {
            console.log(err);
        })


    exportActivities(project)
        .then((res) => {
            let activities = [];
            for (let i = 0; i < res.length; i++) {
                activities.push(buildActivity(JSON.parse(JSON.stringify(res[i]))));
            }
            fs.writeFile('./scripts/files/activities.json', `${JSON.stringify(activities)}`, 'utf8', (e) => {
                console.log('DONE Activities');
            })
        })
        .catch((err) => {
            console.log(err);
        })
}

async function exportProject(projectId) {
    return await storage.projects.getProject({ _id: projectId }, '-__v');
}

async function exportTilesets(projectId) {
    return await storage.tilesets.listTilesets({ project_id: projectId });
}

async function exportActivities(projectId) {
    return await storage.activities.listActivities({ project: projectId, parent: null });
}

function buildProject(source) {
    if (source) {
        let project = {};
        project['_id'] = source._id;
        project['name'] = source.name;
        project['slug'] = source.slug;
        project['description'] = source.description;
        project['projectType'] = source.projectType;
        project['createdBy'] = source.created_by;
        project['wbsId'] = source.wbs_id;
        project['parent'] = source.parent;
        project['owner'] = source.owner;
        project['plannedStartDate'] = source.planned_start_date;
        project['plannedFinishDate'] = source.planned_finish_date;
        project['plannedCost'] = source.planned_cost;
        project['actualStartDate'] = source.actual_start_date;
        project['actualFinishDate'] = source.actual_finish_date;
        project['actualCost'] = source.actual_cost;
        project['duration'] = source.duration;
        project['weightage'] = source.weightage;
        project['scope'] = source.scope;
        // project['snapshot'] = source.snapshot;
        project['projects'] = source.projects;
        project['thumbnail'] = source.owner;
        project['tilesets'] = source.orthomosaics;
        return JSON.stringify(project);
    }
    return null;
}

function buildTileset(source) {
    if (source) {
        let tileset = {};
        tileset['_id'] = source._id;
        tileset['name'] = source.name;
        tileset['description'] = source.description;
        tileset['createdBy'] = source.created_by;
        tileset['project'] = source.project_id;
        tileset['thumbnail'] = source.tileset_thumbnail;
        tileset['zoom'] = source.zoom;
        tileset['pitch'] = source.pitch;
        tileset['center'] = source.center;
        tileset['style'] = source.style;
        tileset['tilesetDate'] = source.tileset_date;
        tileset['annotations'] = source.annotations;
        return tileset;
    }
    return null;
}

function buildActivity(source) {
    if (source) {
        let activity = {};
        activity['_id'] = source._id;
        activity['name'] = source.name;
        activity['description'] = source.description;
        activity['createdBy'] = source.created_by;
        activity['owner'] = source.owner;
        activity['wbsId'] = source.wbs_id;
        activity['isCritical'] = source.is_critical;
        activity['project'] = source.project;
        activity['parent'] = source.parent_activity;
        activity['builder'] = source.builder;
        activity['resources'] = source.resources;
        activity['tags'] = source.tags;
        activity['plannedStartDate'] = source.planned_start_date;
        activity['plannedFinishDate'] = source.planned_finish_date;
        activity['plannedCost'] = source.planned_cost;
        activity['actualStartDate'] = source.actual_start_date;
        activity['actualFinishDate'] = source.actual_finish_date;
        activity['actualCost'] = source.actual_cost;
        activity['duration'] = source.duration;
        activity['weightage'] = source.weightage;
        activity['scope'] = source.scope;
        activity['snapshot'] = source.snapshot;
        activity['activities'] = source.activities;
        activity['manpower'] = source.manpower;
        activity['offsetDays'] = source.offset_days;
        activity['totalFloat'] = source.total_float;
        activity['freeFloat'] = source.free_float;
        activity['predecessors'] = source.predecessors;
        activity['status'] = source.status;
        activity['properties'] = source.properties;
        return activity;
    }
    return null;
}

module.exports = {
    exportProject: exportProject,
    exportTilesets: exportTilesets,
    exportActivities: exportActivities
}