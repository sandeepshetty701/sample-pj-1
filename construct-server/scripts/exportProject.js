'use strict'

const mongodb = require('../config/mongodb');
var fs = require('fs');
const storage = require('../lib/storage').mongoDB;

var Project = mongodb.models.Project;
var Tileset = mongodb.models.Tileset;
var Activity = mongodb.models.Activity;

let project = '5d3189034f7dd7003ee64359';

importDetails();

// exportDetails();

function importDetails() {
    let data = fs.readFileSync('./scripts/files/project.json');
    let project = JSON.parse(data);
    Project.create(project)
    .then(res => {
        console.log(res);
    })
    .catch(err => {
        console.log("============= ERROR ===============");
        console.log(err);
    })

    data = fs.readFileSync('./scripts/files/tilesets.json');
    let tilesets = JSON.parse(data);
    Tileset.insertMany(tilesets)
    .then(res => {
        console.log(res.length);
    })
    .catch(err => {
        console.log("============= ERROR ===============");
        console.log(err);
    })

    data = fs.readFileSync('./scripts/files/activities.json');
    let activities = JSON.parse(data);
    Activity.insertMany(activities)
    .then(res => {
        console.log(res.length);
    })
    .catch(err => {
        console.log("============= ERROR ===============");
        console.log(err);
    })
}

function exportDetails() {
    exportProject(project)
        .then((res) => {
            fs.writeFile('./scripts/files/project.json', buildProject(JSON.parse(JSON.stringify(res))), 'utf8', (e) => {
                console.log('DONE Project');
            })
        })
        .catch((err) => {
            console.log(err);
        })


    exportTilesets(project)
        .then((res) => {
            let tilesets = [];
            for (let i = 0; i < res.length; i++) {
                tilesets.push(buildTileset(JSON.parse(JSON.stringify(res[i]))));
            }
            fs.writeFile('./scripts/files/tilesets.json', `${JSON.stringify(tilesets)}`, 'utf8', (e) => {
                console.log('DONE Tilesets');
            })
        })
        .catch((err) => {
            console.log(err);
        })


    exportActivities(project)
        .then((res) => {
            let activities = [];
            for (let i = 0; i < res.length; i++) {
                activities.push(buildActivity(JSON.parse(JSON.stringify(res[i]))));
            }
            fs.writeFile('./scripts/files/activities.json', `${JSON.stringify(activities)}`, 'utf8', (e) => {
                console.log('DONE Activities');
            })
        })
        .catch((err) => {
            console.log(err);
        })
}

async function exportProject(projectId) {
    return await storage.projects.getProject({ _id: projectId }, '-__v');
}

async function exportTilesets(projectId) {
    return await storage.tilesets.listTilesets({ project_id: projectId });
}

async function exportActivities(projectId) {
    return await storage.activities.listActivities({ project: projectId, parent: null });
}

function buildProject(source) {
    if (source) {
        let project = {};
        project['_id'] = source._id;
        project['name'] = source.name;
        project['slug'] = source.slug;
        project['description'] = source.description;
        project['projectType'] = source.projectType;
        project['createdBy'] = source.created_by;
        project['wbsId'] = source.wbs_id;
        project['parent'] = source.parent;
        project['owner'] = source.owner;
        project['plannedStartDate'] = source.planned_start_date;
        project['plannedFinishDate'] = source.planned_finish_date;
        project['plannedCost'] = source.planned_cost;
        project['actualStartDate'] = source.actual_start_date;
        project['actualFinishDate'] = source.actual_finish_date;
        project['actualCost'] = source.actual_cost;
        project['duration'] = source.duration;
        project['weightage'] = source.weightage;
        project['scope'] = source.scope;
        // project['snapshot'] = source.snapshot;
        project['projects'] = source.projects;
        project['thumbnail'] = source.owner;
        project['tilesets'] = source.orthomosaics;
        return JSON.stringify(project);
    }
    return null;
}

function buildTileset(source) {
    if (source) {
        let tileset = {};
        tileset['_id'] = source._id;
        tileset['name'] = source.name;
        tileset['description'] = source.description;
        tileset['createdBy'] = source.created_by;
        tileset['project'] = source.project_id;
        tileset['thumbnail'] = source.tileset_thumbnail;
        tileset['zoom'] = source.zoom;
        tileset['pitch'] = source.pitch;
        tileset['center'] = source.center;
        tileset['style'] = source.style;
        tileset['tilesetDate'] = source.tileset_date;
        tileset['annotations'] = source.annotations;
        return tileset;
    }
    return null;
}

function buildActivity(source) {
    if (source) {
        let activity = {};
        activity['_id'] = source._id;
        activity['name'] = source.name;
        activity['description'] = source.description;
        activity['createdBy'] = source.created_by;
        activity['owner'] = source.owner;
        activity['wbsId'] = source.wbs_id;
        activity['isCritical'] = source.is_critical;
        activity['project'] = source.project;
        activity['parent'] = source.parent_activity;
        activity['builder'] = source.builder;
        activity['resources'] = source.resources;
        activity['tags'] = source.tags;
        activity['plannedStartDate'] = source.planned_start_date;
        activity['plannedFinishDate'] = source.planned_finish_date;
        activity['plannedCost'] = source.planned_cost;
        activity['actualStartDate'] = source.actual_start_date;
        activity['actualFinishDate'] = source.actual_finish_date;
        activity['actualCost'] = source.actual_cost;
        activity['duration'] = source.duration;
        activity['weightage'] = source.weightage;
        activity['scope'] = source.scope;
        activity['snapshot'] = source.snapshot;
        activity['activities'] = source.activities;
        activity['manpower'] = source.manpower;
        activity['offsetDays'] = source.offset_days;
        activity['totalFloat'] = source.total_float;
        activity['freeFloat'] = source.free_float;
        activity['predecessors'] = source.predecessors;
        activity['status'] = source.status;
        activity['properties'] = source.properties;
        return activity;
    }
    return null;
}

module.exports = {
    exportProject: exportProject,
    exportTilesets: exportTilesets,
    exportActivities: exportActivities
}