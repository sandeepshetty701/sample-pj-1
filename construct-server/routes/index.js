'use strict';

var express = require('express');

var router = express.Router();

var middlewares = require('../middlewares');

var errorHandler = require('../validators/errorHandler');

var organisationRoutes = require('./organisationRoutes');

var projectRoutes = require('./projectRoutes');

var activityRoutes = require('./activityRoutes');

var annotationRoutes = require('./annotationRoutes');

var awsRoutes = require('./awsRoutes');

var tilesetRoutes = require('./tilesetRoutes')

router.use('/organisations', organisationRoutes);

router.use(middlewares.authFunc.checkUserToken);

router.use('/projects', projectRoutes);

router.use('/activities', activityRoutes);

router.use('/annotations', annotationRoutes);

router.use('/aws', awsRoutes);

router.use('/tilesets', tilesetRoutes);

router.all('*', function(req, res){
    res.status(400)
    res.send('Invalid route');
});

module.exports = router;
