'use strict';

var express = require('express');

var router = express.Router();

var tilesetController = require('../controllers/tilesetController');

// Add a new tileset to a project
router.post('', tilesetController.create_tileset);

// List all the tilesets associated to the project
router.get('', tilesetController.list_tilesets);

// Get tileset info by object id
router.get('/:tid', tilesetController.get_tileset_by_id);

// Update tileset info
router.put('/:tid', tilesetController.update_tileset);

// Remove tileset and de-assign from project
router.delete('/:tid', tilesetController.delete_tileset);

module.exports = router;
