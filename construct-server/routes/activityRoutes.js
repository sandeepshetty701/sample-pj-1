'use strict';

var express = require('express');

var router = express.Router({mergeParams: true});

var activityController = require('../controllers/activityController');

/*	create an activity under a given project
		REQUEST
		{
			"name": "Activity name", (*Required)
			"description": "Couple of lines description",
			"owner": <ObjectId of the user who owns this project>,
			"created_by": <Fetch it from the user token>
			"project": <ObjectId of the project under which the activity is created>, Fetch it from the path param (*Required)
			"parent_activity": <ObjectId of the parent activity>,
			"builder": "Tricon Infra",
			"completed_percentage": -1, // -1 for Just created projects
			"duration": 64, // in days
			"planned_start_date": 12/12/2018, (*Required)
			"planned_finish_date": 12/6/2019, (*Required)
			"actual_cost": 23456789, // in INR
			"predecessors": [{
				
			}]
		}
		RESPONSE - 201 Created
*/
router.post('/', activityController.create_new_activity);

/*	get all activities for the project
		RESPONSE - JSONObject of all the activities with parent child hierarchy, under a given project
*/
router.get('/hierarchy', activityController.get_activity_hierarchy);

/*	get all activities gantt
		RESPONSE - JSONObject of all the activities with parent child hierarchy, under a given project
*/
router.get('/gantt', activityController.get_activity_gantt);

/*	get all activities for the project
		RESPONSE - JSONObject of all the activities with parent child hierarchy, under a given project
*/
router.get('/', activityController.get_activities);

/*	get an activity by id
		REQUEST - project :id and :activity_id as path params
		RESPONSE - JSONObject of the activity as response
*/
router.get('/:activity_id', activityController.get_activity_by_id);

/*	update an activity by id
		REQUEST - project :id and :activity_id as path params
		RESPONSE - updated JSONObject of the activity as response
*/
router.put('/:activity_id', activityController.update_activity);

/*	update an activity snapshot by id
		REQUEST - project :id and :activity_id as path params
		RESPONSE - updated JSONObject of the activity as response
*/
router.put('/:activity_id/snapshot', activityController.update_activity_progress);

/*	Add predecessor / dependency
		REQUEST - project :id and :activity_id as path params
		{
			"predecessor": {
	
			}
		}
		RESPONSE - updated JSONObject of the activity as response
*/
router.put('/:activity_id', activityController.update_activity);

/*	delete an activity by id
		REQUEST - project :id and :activity_id as path params
		RESPONSE - 204 Deleted.
*/
router.delete('/:activity_id', activityController.delete_activity);

module.exports = router;
