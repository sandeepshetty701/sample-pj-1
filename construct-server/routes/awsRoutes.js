'use strict';

var express = require('express');

var multer = require('multer');

var router = express.Router();

var awsController = require('../controllers/awsController');

var storage = multer.memoryStorage({
    destination: function (req, file, callback) {
        callback(null, '');
    }
});

var multipleUpload = multer({ storage: storage, limits: { fileSize: 500 * 1024 * 1024 } }).array('file');

// Upload file to s3
router.post('', awsController.s3Upload);

router.post('/upload-files', multipleUpload, awsController.uploadFiles);

module.exports = router;
