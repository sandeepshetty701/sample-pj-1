'use strict';

var express = require('express');

var router = express.Router();

var annotationController = require('../controllers/annotationController');

// Add a new annotation to an orthomosaic
router.post('', annotationController.createAnnotation);

// List all the annotations associated to the orthomosaic
router.get('', annotationController.listAnnotations);

// Get annotation info by object id
router.get('/:annoId', annotationController.getAnnotationDetails);

// Update annotation info
router.put('/:annoId', annotationController.updateAnnotation);

// Remove annotation and de-assign from orthomosaic
router.delete('/:annoId', annotationController.deleteAnnotation);

module.exports = router;