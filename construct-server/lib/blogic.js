'use strict'

const storage = require('./storage');
require('dotenv').config();
const awsUtils = require('../utils/aws');
const rp = require('request-promise');
const mongodb = require('../config/mongodb');
const Project = mongodb.models.Project;
const excelToJson = require('convert-excel-to-json');
const fs = require('fs');
const converter = require('json-2-csv');

let organisations = {

    createOrganisation: async function (base_url, body) {
        try {
            let userObj = Object.assign({}, body);
            let orgObj = {
                name: body.orgName,
                email: body.email
            };

            delete userObj.orgName;
            let createUserResponse = await registerUser(base_url, userObj);
            if (!createUserResponse.success) {
                return {
                    success: false,
                    message: createUserResponse.message
                }
            }

            let organisation = await storage.organisations.createOrganisation(orgObj);
            let entityRole = {
                entity: organisation._id,
                entityType: 'construct.organisation',
                role: process.env.ORG_OWNER_ROLE,
                user: createUserResponse.result._id
            }
            let entityRoleResponse = await entityRoles.createEntityRole(entityRole, '3rdi.internal.api');
            if (!entityRoleResponse.success) {
                return {
                    success: false,
                    message: entityRoleResponse.message
                }
            }

            return {
                success: true,
                message: `Organisation ${organisation.name} has been registered successfully!`,
                result: organisation
            };
        } catch (err) {
            if (err.code === 11000) {
                err.code = 409,
                    err.message = `Organisation with name "${body.orgName}" already exists`
            }
            throw (err);
        }
    },

    getOrganisations: async function (query) {
        try {
            let populate = getPopulateFields(query);
            delete query.populate;
            let organisations = await storage.organisations.getOrganisations(query, populate);
            return {
                success: true,
                result: organisations
            };
        } catch (err) {
            throw (err);
        }
    },

    getOrganisation: async function (query) {
        try {
            let populate = getPopulateFields(query);
            delete query.populate;
            let organisation = await storage.organisations.getOrganisation(query, '-__v', populate);
            return {
                success: true,
                result: organisation
            };
        } catch (err) {
            throw (err);
        }
    },

    updateOrganisation: async function (query, body) {
        try {
            let organisation = await storage.organisations.updateOrganisation(query, body);
            return {
                success: true,
                result: organisation
            };
        } catch (err) {
            throw (err);
        }
    },

    deleteOrganisation: async function (query) {
        try {
            let organisation = await storage.organisations.deleteOrganisation(query);
            return {
                success: true,
                result: `${organisation.name} organisation has been removed!`
            };
        } catch (err) {
            throw (err);
        }
    },

    uploadLogo: async function (orgId, file) {
        try {
            if (!file) {
                throw({
                    code: 400,
                    message: `No files were uploaded.`
                });
            }

            let extension = file.originalname.split('.')[file.originalname.split('.').length - 1];
            if (['png', 'jpg', 'jpeg'].indexOf(extension) === -1) {
                throw({
                    code: 400,
                    message: `Only .png .jpg .jpeg formats are supported.`
                });
            }

            let path = `organisations/${orgId}/logo.${extension}`;
            let responseData = await awsUtils.uploadSingleFile('xyz-icons', path, file);
            return {
                success: true,
                message: responseData
            };
        } catch (err) {
            throw (err);
        }
    },

    inviteUser: async function (base_url, fromUser, orgId, email, roleId) {
        try {
            let organisation = await storage.organisations.getOrganisation({
                _id: orgId
            }, '-__v');
            if (!organisation) {
                throw({
                    code: 404,
                    message: `Requested Organisation does not exist`
                });
            }

            let invitingUser = await getUserProfile(fromUser.token);
            if(invitingUser.success) {
                invitingUser = invitingUser.result;
            }
            let userResponse = await getUser(email, fromUser.token);
            if (userResponse.success) {
                let user = userResponse.result;
                if (user) {
                    return await this.assignUser(orgId, user._id, roleId, fromUser.token);
                } else {
                    // Checking if invitation already exists for this user
                    let invitationResponse = await invitations.getInvitationByEmail(email, orgId,
                        'construct.organisation', fromUser.token);
                    if (invitationResponse.result) {
                        throw ({
                            code: 409,
                            message: `Invitation already exists`
                        });
                    }
                    let invitation = {
                        entity: orgId,
                        from: fromUser._id,
                        to: email,
                        entityType: 'construct.organisation',
                        role: roleId
                    }

                    //  Creating an invitation
                    let creatInvitationResponse = await invitations.createInvitation(invitation, fromUser.token);
                    if (!creatInvitationResponse.success) {
                        return {
                            success: false,
                            message: creatInvitationResponse.message
                        }
                    }
                    let createdInvitation = creatInvitationResponse.result;

                    //  Sending email
                    let emailResponse = await sendEmail({
                        email: email,
                        template_name: 'invite-user',
                        template_data: {
                            NAME: email,
                            INVITE_SENDER_NAME: invitingUser.fullName,
                            INVITE_SENDER_EMAIL: invitingUser.email,
                            INVITE_SENDER_ORGANISATION: organisation.name,
                            PROJECT_NAME: '',
                            ACTION_URL: base_url + process.env.INVITATION_URL + createdInvitation._id
                        }
                    }, fromUser.token);

                    if (!emailResponse.success) {
                        return {
                            success: false,
                            message: emailResponse.message
                        }
                    } else {
                        return {
                            success: true,
                            message: `Invitation has been sent to ${email}`
                        }
                    }
                }
            } else {
                return {
                    success: false,
                    message: userResponse.message
                }
            }
        } catch (err) {
            console.log(err)
            throw (err);
        }

    },

    assignUser: async function (orgId, userId, roleId, token) {
        try {

            let entityRoleResponse = await entityRoles.createEntityRole({
                entity: orgId,
                user: userId,
                role: roleId,
                entityType: 'construct.organisation'
            }, token);

            if (!entityRoleResponse.success) {
                return {
                    success: false,
                    message: entityRoleResponse.message
                }
            }

            return {
                success: true,
                message: 'Assigned user to organisation successfully!'
            };
        } catch (err) {
            throw (err);
        }
    },

    deassignUser: async function (orgId, userId, token) {
        try {

            let entityRoleResponse = await entityRoles.deleteEntityUser(userId, [orgId],
                'construct.organisation', token);

            if (!entityRoleResponse.success) {
                return {
                    success: false,
                    message: entityRoleResponse.message
                }
            }

            return {
                success: true,
                message: 'De-Assigned user from organisation successfully!'
            };
        } catch (err) {
            throw (err);
        }
    },

    changeRole: async function (orgId, userId, roleId, token) {
        try {

            let entityRoleResponse = await entityRoles.updateEntityUser(userId, [orgId],
                'construct.organisation', roleId, token);

            if (!entityRoleResponse.success) {
                return {
                    success: false,
                    message: entityRoleResponse.message
                }
            }

            return {
                success: true,
                message: 'Changed role successfully!'
            };
        } catch (err) {
            throw (err);
        }
    },

    getMyOrganisations: async function (user, query) {
        try {
            if (!query) {
                return {
                    success: false,
                    message: 'Please provide an organisation ID'
                };
            }

            let _query = {};
            let assignedOrganisations = await entityRoles.getEntityRoles(`user=${user.user}&entityType=construct.organisation`, user.token);
            if (assignedOrganisations.success && assignedOrganisations.result) {
              let ids = [];
              for (let i = 0; i < assignedOrganisations.result.length; i++) {
                ids.push(assignedOrganisations.result[i].entity);
              }
              _query = {
                _id: {
                  $in: ids
                }
              };
            }

            let organisations = await storage.organisations.getOrganisations(_query);
            return {
                success: true,
                result: organisations
            };
        } catch (err) {
            throw (err);
        }
    }

}

var projects = {

    listMyProjectsFlat: async function (user) {
        try {
            let _query = {};
            let assignedProjects = await entityRoles.getEntityRoles(`user=${user.user}&entityType=construct.project`, user.token);
            if (assignedProjects.success && assignedProjects.result) {
              let ids = [];
              for (let i = 0; i < assignedProjects.result.length; i++) {
                ids.push(assignedProjects.result[i].entity);
              }
              _query = {
                _id: {
                  $in: ids
                }
              };
            }

            let projects = await storage.projects.getProjects(_query, 'name parent projects thumbnail tilesets snapshot');
            return {
                success: true,
                result: projects
            };
        } catch (err) {
            throw (err);
        }
    },

    getProject: async function (projectId, query) {
        try {
            let populate = getPopulateFields(query);
            if (!populate) {
                populate = [
                    {
                        path: 'parent', select: '-_id name description progress snapshot createdAt users',
                        populate: [{ path: 'users.role', select: 'name' }, { path: 'users.user', select: 'fullName email mobile' }]
                    },
                    { path: 'createdBy', select: 'fullName email mobile -_id' },
                    {
                        path: 'projects', select: '_id name description progress snapshot createdAt users geometry',
                        populate: [{ path: 'users.role', select: 'name' }, { path: 'users.user', select: 'fullName email mobile' }]
                    },
                    { path: 'tilesets', select: '-__v', options: { sort: { tilesetDate: 1 } }, populate: { path: 'annotations', select: '-__v' } }
                ];
            }
            delete query.populate;
            let project = await storage.projects.getProject({ _id: projectId }, '-__v', populate);
            return {
                success: true,
                result: project
            };
        } catch (err) {
            throw (err);
        }
    },

    getProjectGantt: async function (projectId) {
        try {
            let project = await storage.projects.getProject({
                _id: projectId
            }, 'name parent');
            while (project && project.parent) {
                project = await storage.projects.getProject({
                    _id: project.parent
                }, 'name parent');
            }

            let query = { project: project._id }
            let populate = getPopulateFields(query);
            if (!populate) {
                populate = [{ path: 'createdBy', select: '-__v' }]
            }
            delete query.populate;
            let activities = await storage.activities.listActivities(query, populate);
            let ganttItems = [];
            for (let i = 0; i < activities.length; i++) {
                let dependency = '';
                if (activities[i].predecessors) {
                    let predecessors = JSON.parse(JSON.stringify(activities[i].predecessors));
                    for (let j = 0; j < predecessors.length; j++) {
                        dependency += predecessors[j].wbsId + predecessors[j].relation + predecessors[j].offset;
                        if (j != predecessors.length - 1) {
                            dependency += ',';
                        }
                    }
                }
                let ganttItem = __createGanttRow(activities[i], '', dependency);
                ganttItems.push(ganttItem);
            }

            return {
                success: true,
                result: ganttItems
            };
        } catch (err) {
            throw(err);
        }
    },

    downloadProjectPlan: async function (projectId) {
        try {

            let project = await storage.projects.getProject({
                _id: projectId
            }, 'name parent');

            while (project.parent) {
                project = await storage.projects.getProject({
                    _id: project.parent
                }, 'name parent');
            }

            let query = {
                project: project._id
            }

            let populate = getPopulateFields(query);
            // if (!populate) {
            //     populate = [{ path: 'createdBy', select: '-__v' }]
            // }
            delete query.populate;
            let activities = await storage.activities.listActivities(query, populate);

            for(let i = 0; i < activities.length; i++) {
                activities[i].wbsId = 'wbs-' + activities[i].wbsId;
            }

            let options = {
                delimiter : {
                    wrap  : '"', // Double Quote (") character
                    field : ',', // Comma field delimiter
                    eol   : '\n' // Newline delimiter
                },
                emptyFieldValue: '',
                keys: ['wbsId', 'name', 'scope.quantity', 'scope.units', 'weightage', 'plannedStartDate',
                    'plannedFinishDate', 'actualStartDate', 'actualFinishDate', 'progress.actual'
                ],
                trimHeaderFields: true
            };
            let result = await converter.json2csvAsync(activities, options);
            result = result.replace('scope.quantity', 'quantity');
            result = result.replace('scope.units', 'units');
            result = result.replace('progress.actual', 'progress');

            var bucketParams = {
                Bucket: process.env.S3_CONSTRUCT_BUCKET,
                ACL: 'public-read',
                Key: 'projects/' + projectId + '/project-plan.csv',
                Body: result
            };

            await awsUtils.s3_upload(bucketParams);

            return {
                success: true,
                result: `${process.env.S3_CONSTRUCT_BUCKET}/projects/${projectId}/project-plan.csv`
            };
        } catch (err) {
            throw (err);
        }
    },

    uploadProjectPlan: async function (user, projectId, file) {
        try {
            if (!file) {
                throw({
                    code: 400,
                    message: `No files were uploaded.`
                });
            }

            if (['csv'].indexOf(file.originalname.split('.')[file.originalname.split('.').length - 1]) === -1) {
                throw({
                    code: 400,
                    message: `Only .csv format is supported.`
                });
            }

            let project = await storage.projects.getProject({
                _id: projectId
            }, 'name parent');

            if(!project) {
                throw({
                    code: 404,
                    message: `Project does not exist`
                })
            }

            while (project.parent) {
                project = await storage.projects.getProject({
                    _id: project.parent
                }, 'name parent');
            }

            const result = await converter.csv2jsonAsync(file.buffer.toString());

            let activities = [];
            for(let i = 0; i < result.length; i++) {
                let row = result[i];
                row['scope'] = {
                    quantity: row.quantity,
                    units: row.units
                };
                row.wbsId = row.wbsId.replace('wbs-', '');
                let split = row.wbsId.lastIndexOf('.');
                if(split > -1) {
                    row['parentWbs'] = row.wbsId.substring(0, split);
                }
                delete row.quantity;
                delete row.units;
                row['createdBy'] = `${user}`;
                row['owner'] = `${user}`;
                // row['project'] = projectId;
                row['project'] = project._id;
                activities.push(row);
            }
            await storage.activities.deleteActivity({project: projectId})
            let activitiesResponse = await storage.activities.bulkCreateActivities(activities);
            if(activitiesResponse) {
                return {
                    success: true,
                    result: `Succesfully created Project Plan with ${activitiesResponse.length} activities`
                };
            }
        } catch (err) {
            console.log(err)
            throw (err);
        }
    },

    createProject: async function (user, body) {
        try {
            var parentProject = body.parent;
            var inDuplicateName = await storage.projects.getProject({
                parent: parentProject, name: body.name
            });
            console.log(body.parent, body.name, inDuplicateName);
            if (inDuplicateName) {
                throw ({
                    code: 409,
                    message: `Project with name ${body.name} already exists`
                });
            }

            var usersSet = new Set();
            var newProject = new Project(body);
            newProject.slug = newProject.name.replace(/ +/g, '_');

            //  Calculating the duration if planned-start and planned-finish dates are provided
            if (newProject.planned_start_date && newProject.planned_finish_date) {
                var date_format_psd = new Date(newProject.planned_start_date);
                var date_format_pfd = new Date(newProject.planned_finish_date);
                var diff = Math.abs(date_format_pfd.getTime() - date_format_psd.getTime())

                newProject.duration = Math.ceil(diff / (1000 * 3600 * 24))
            }

            //  Assigning the users (except current user) from the parent projects hierarchy with same roles
            // @ToDo

            //  Setting the createdBy to current user
            newProject.createdBy = user.user;

            //  Setting the createdBy as owner if value for owner is not provided in request
            if (!newProject.owner) {
                newProject.owner = user.user;
            }

            //  Setting the current user as project admin
            //  @ToDo

            var savedProject = await newProject.save();
            if (savedProject.parent) {
                await storage.projects.updateProject({
                    _id: savedProject.parent
                }, { $addToSet: { projects: savedProject._id } });
            }

            let entityRole = {
                entity: savedProject._id,
                entityType: 'construct.project',
                role: process.env.PROJECT_ADMIN_ROLE,
                user: user.user
            }
            let entityRoleResponse = await entityRoles.createEntityRole(entityRole, user.token);
            if (!entityRoleResponse.success) {
                return {
                    success: false,
                    message: entityRoleResponse.message
                }
            }

            var bucketParams = {
                Bucket: process.env.S3_CONSTRUCT_BUCKET,
                ACL: 'public-read',
                Key: 'projects/' + savedProject._id + '/',
                Body: ''
            };

            await awsUtils.s3_upload(bucketParams);

            return {
                success: true,
                result: savedProject
            };
        } catch (err) {
            throw (err);
        }
    },

    updateProject: async function (query, body) {
        try {
            var parentProject = body.parent;
            var inDuplicateName = await storage.projects.getProject({
                parent: parentProject, name: body.name
            });
            if (inDuplicateName) {
                throw ({
                    code: 409,
                    message: `Project with name ${body.name} already exists`
                });
            }
            if (body.name) {
                body.slug = body.name.replace(/ +/g, '');
            }

            let project = await storage.projects.updateProject(query, body, {
                new: true
            });
            return {
                success: true,
                result: project
            };
        } catch (err) {
            throw (err);
        }
    },

    deleteProject: async function (projectId) {
        try {
            let project = await storage.projects.getProject({ _id: projectId });
            await storage.annotations.removeAnnotation({ project: projectId });
            if (project.parent) {
                await storage.projects.updateProject({ _id: project.parent }, { $pull: { projects: projectId } });
            }
            await storage.tilesets.removeTileset({ project: projectId });
            await storage.projects.removeProject({ _id: projectId });
            await awsUtils.s3_delete_folder(process.env.S3_CONSTRUCT_BUCKET, `projects/${projectId}`);

            return {
                success: true,
                result: project
            };
        } catch (err) {
            throw (err);
        }
    },

    inviteUser: async function (base_url, fromUser, projectId, email, roleId) {
        try {
            let project = await storage.projects.getProject({
                _id: projectId
            }, '-__v');
            if (!project) {
                throw({
                    code: 404,
                    message: `Requested Project does not exist`
                });
            }

            let invitingUser = await getUserProfile(fromUser.token);
            if(invitingUser.success) {
                invitingUser = invitingUser.result;
            }
            let userResponse = await getUser(email, fromUser.token);
            if (userResponse.success) {
                let user = userResponse.result;
                if (user) {
                    return await this.assignUser(projectId, user._id, roleId, fromUser.token);
                } else {
                    // Checking if invitation already exists for this user
                    let invitationResponse = await invitations.getInvitationByEmail(email, projectId,
                        'construct.project', fromUser.token);
                    if (invitationResponse.result) {
                        throw ({
                            code: 409,
                            message: `Invitation already exists`
                        });
                    }
                    let invitation = {
                        entity: projectId,
                        from: fromUser._id,
                        to: email,
                        entityType: 'construct.project',
                        role: roleId
                    }

                    //  Creating an invitation
                    let creatInvitationResponse = await invitations.createInvitation(invitation, fromUser.token);
                    if (!creatInvitationResponse.success) {
                        return {
                            success: false,
                            message: creatInvitationResponse.message
                        }
                    }
                    let createdInvitation = creatInvitationResponse.result;

                    //  Sending email
                    let emailResponse = await sendEmail({
                        email: email,
                        template_name: 'invite-user',
                        template_data: {
                            NAME: email,
                            INVITE_SENDER_NAME: invitingUser.fullName,
                            INVITE_SENDER_EMAIL: invitingUser.email,
                            INVITE_SENDER_ORGANISATION: '',
                            PROJECT_NAME: project.name,
                            ACTION_URL: base_url + process.env.INVITATION_URL + createdInvitation._id
                        }
                    }, fromUser.token);

                    if (!emailResponse.success) {
                        return {
                            success: false,
                            message: emailResponse.message
                        }
                    } else {
                        return {
                            success: true,
                            message: `Invitation has been sent to ${email}`
                        }
                    }
                }
            } else {
                return {
                    success: false,
                    message: userResponse.message
                }
            }
        } catch (err) {
            throw (err);
        }
    },

    assignUser: async function (projectId, userId, roleId, token) {
        try {
            let projectIds = await storage.projects.getProjectAndChildProjectIds(projectId);
            let mProjects = [];
            projectIds.forEach(res => {
                mProjects = res.projectIds;
            });
            let mRoles = [];
            for(let i = 0; i < mProjects.length; i++) {
                mRoles.push({
                    entity: mProjects[i],
                    user: userId,
                    role: roleId,
                    entityType: 'construct.project'
                });
            }
            let entityRoleResponse = await entityRoles.createEntityRole(mRoles, token);

            if (!entityRoleResponse.success) {
                return {
                    success: false,
                    message: entityRoleResponse.message
                }
            }

            return {
                success: true,
                message: 'Assigned user to project successfully!'
            };
        } catch (err) {
            throw (err);
        }
    },

    deassignUser: async function (projectId, userId, token) {
        try {
            let projectIds = await storage.projects.getProjectAndChildProjectIds(projectId);
            let mProjects = [];
            projectIds.forEach(res => {
                mProjects = res.projectIds;
            });

            let entityRoleResponse = await entityRoles.deleteEntityUser(userId, mProjects,
                'construct.project', token);

            if (!entityRoleResponse.success) {
                return {
                    success: false,
                    message: entityRoleResponse.message
                }
            }

            return {
                success: true,
                message: 'De-Assigned user from project successfully!'
            };
        } catch (err) {
            throw (err);
        }
    },

    changeRole: async function (projectId, userId, roleId, token) {
        try {
            let projectIds = await storage.projects.getProjectAndChildProjectIds(projectId);
            let mProjects = [];
            projectIds.forEach(res => {
                mProjects = res.projectIds;
            });

            let entityRoleResponse = await entityRoles.updateEntityUser(userId, mProjects,
                'construct.project', roleId, token);

            if (!entityRoleResponse.success) {
                return {
                    success: false,
                    message: entityRoleResponse.message
                }
            }

            return {
                success: true,
                message: 'Changed role successfully!'
            };
        } catch (err) {
            throw (err);
        }
    }

}

var activities = {

    getActivities: async function (query) {
        try {
            console.log(query)
            let project = await storage.projects.getProject({
                _id: query.project
            }, 'name parent');

            if(!project) {
                throw({
                    code: 404,
                    message: `Project does not exist`
                })
            }

            while (project.parent) {
                project = await storage.projects.getProject({
                    _id: project.parent
                }, 'name parent');
            }

            query['project'] = project._id;

            let populate = getPopulateFields(query);
            // if (!populate) {
            //     populate = [{ path: 'createdBy', select: '-__v' }]
            // }
            delete query.populate;
            let activities = await storage.activities.listActivities(query, populate);
            return {
                success: true,
                result: activities
            };
        } catch (err) {
            console.log(err)
            throw (err);
        }
    },

    getGanttChart: async function (projectId) {
        try {
            let project = await storage.projects.getProject({
                _id: projectId
            }, 'name parent');

            while (project.parent) {
                project = await storage.projects.getProject({
                    _id: project.parent
                }, 'name parent');
            }

            let query = { project: project._id }
            let populate = getPopulateFields(query);
            if (!populate) {
                populate = [{ path: 'createdBy', select: '-__v' }]
            }
            delete query.populate;
            let activities = await storage.activities.listActivities(query, populate);
            let activityMap = {};
            for (let i = 0; i < activities.length; i++) {
                activityMap[activities[i]._id] = activities[i].wbsId;
            }

            let ganttItems = [];
            for (let i = 0; i < activities.length; i++) {
                let dependency = '';
                if (activities[i].predecessors) {
                    let predecessors = JSON.parse(JSON.stringify(activities[i].predecessors));
                    for (let j = 0; j < predecessors.length; j++) {
                        dependency += predecessors[j].wbsId + predecessors[j].relation + predecessors[j].offset;
                        if (j != predecessors.length - 1) {
                            dependency += ',';
                        }
                    }
                }
                let ganttItem = __createGanttRow(activities[i], activityMap[activities[i].parent], dependency);
                ganttItems.push(ganttItem);
            }

            return {
                success: true,
                result: ganttItems
            };
        } catch (err) {
            return throwErrorMessage(err);
        }
    },

    createActivity: async function (body) {
        try {
            let activity = await storage.activities.createActivity(body);
            return {
                success: true,
                result: activity
            };
        } catch (err) {
            throw (err);
        }
    },

    create_new_activity: async function (body, projectId, userId) {
        try {
            let date = new Date();
            let snapshotDate = new Date((date.getMonth() + 1) + '-' + date.getDate() + '-' + date.getFullYear());

            if(body.plannedStartDate) {
                body.plannedStartDate = new Date(body.plannedStartDate)
            }

            if(body.plannedFinishDate) {
                body.plannedFinishDate = new Date(body.plannedFinishDate)
            }

            let project = await storage.projects.getProject({
                _id: projectId
            }, 'name parent');

            if (!project) {
                throw ({
                    code: 404,
                    message: `Project does not exist`
                })
            }

            while (project.parent) {
                project = await storage.projects.getProject({
                    _id: project.parent
                }, 'name parent');
            }
            body['project'] = project._id;
            let snapshot = {
                date: snapshotDate
            };

            snapshot['planned'] = body.planned_progress ? body.planned_progress : 0;
            snapshot['actual'] = body.actual_progress ? body.actual_progress : 0;

            delete body.planned_progress;
            delete body.actual_progress;

            let activity = await storage.activities.createActivity(body, userId);
            
            let query = {
                _id: activity._id,
                'snapshot.date': snapshotDate
            };
            let updateActivity = await activities.updateActivityProgress(query, snapshot, snapshotDate);

            return {
                success: true,
                result: updateActivity
            };
        } catch (err) {
            if (err.code === 11000) {
                err.code = 409;
                err.message = "wbsId already exist in this project";
              }
            throw (err);
        }
    },

    createBulkActivities: async function (body) {
        try {
            let activities = await storage.activities.bulkCreateActivities(body);
            return {
                success: true,
                result: activities
            };
        } catch (err) {
            throw (err);
        }
    },

    updateActivity: async function (query, body) {
        try {
            let activity = await storage.activities.updateActivity(query, body, {
                new: true
            });
            return {
                success: true,
                result: activity
            };
        } catch (err) {
            throw (err);
        }
    },

    updateActivityProgress: async function (query, reqBody, date) {
        try {
            let updateBody = {};
            if (reqBody.actual !== undefined) {
                updateBody['snapshot.$.actual'] = reqBody.actual;
            }
            if (reqBody.planned !== undefined) {
                updateBody['snapshot.$.planned'] = reqBody.planned;
            }
            let body = reqBody;
            body.date = date;

            let mUpdate = await storage.activities.updateActivity(query, {
                '$set': updateBody
            }, true);
            if (!mUpdate) {
                mUpdate = await storage.activities.updateActivity({_id: query._id}, {
                    $push: {
                        snapshot: {
                            $each: [body],
                            $sort: {
                                date: 1
                            }
                        }
                    }
                });
            }
            
            let updatedActivity = mUpdate;
            let parentWbs = getParentWbs(updatedActivity.wbsId);
            let progress;
            while (parentWbs && parentWbs !== "0") {
                progress = await storage.activities.getActivityProgressForDate(date, {
                    wbsId: parentWbs, project: updatedActivity.project
                });

                mUpdate = await storage.activities.updateActivity({
                    wbsId: parentWbs, project: updatedActivity.project
                }, {
                        $pull: {
                            snapshot: {
                                date: date
                            }
                        }
                    }, {
                        new: true
                    });
                mUpdate = await storage.activities.updateActivity({
                    wbsId: parentWbs, project: updatedActivity.project
                }, {
                        $push: {
                            snapshot: {
                                $each: [progress],
                                $sort: {
                                    date: 1
                                }
                            }
                        }
                    }, {
                        new: true
                    });

                parentWbs = getParentWbs(mUpdate.wbsId);

            }

            // let parentProject = mUpdate.project;
            // progress = await storage.activities.getActivityProgressForDate(date, {
            //     parentWbs: null,
            //     project: parentProject
            // });
            // mUpdate = await storage.projects.updateProject({
            //     _id: parentProject
            // }, {
            //         $pull: {
            //             snapshot: {
            //                 date: date
            //             }
            //         }
            //     });
            // mUpdate = await storage.projects.updateProject({
            //     _id: parent_project
            // }, {
            //         $push: {
            //             snapshot: {
            //                 $each: progress,
            //                 $sort: {
            //                     date: 1
            //                 }
            //             }
            //         }
            //     });
            // parent_project = mUpdate.project;

            // while (parent_project) {
            //     progress = await storage.projects.getProjectProgressForDate(date, parent_project);

            //     mUpdate = await storage.projects.updateProject({
            //         _id: parent_project
            //     }, {
            //             $pull: {
            //                 snapshot: {
            //                     date: date
            //                 }
            //             }
            //         });
            //     mUpdate = await storage.projects.updateProject({
            //         _id: parent_project
            //     }, {
            //             $push: {
            //                 snapshot: {
            //                     $each: progress,
            //                     $sort: {
            //                         date: 1
            //                     }
            //                 }
            //             }
            //         });

            //     parent_project = mUpdate.project;
            // }

            return updatedActivity;
        } catch (err) {
            throw (err);
        }
    },

    deleteActivity: async function (query) {
        try {
            let activity = await storage.activities.getActivity(query);
            if (!activity) {
                throw ({
                    code: 404,
                    message: `Activity does not exist`
                })
            }
            let deleteQuery = {
                "wbsId" : { $regex: `^${activity.wbsId}.*`} ,
                "project": activity.project
            }
            await storage.activities.deleteActivity(deleteQuery);
            return {
                success: true,
                result: `${activity.name} and its subactivities has been removed!`
            };
        } catch (err) {
            throw (err);
        }
    }
}

var annotations = {

    listAnnotations: async function (query) {
      try {
        let populate = getPopulateFields(query);
        if (!populate) {
          populate = [{
            path: 'createdBy',
            select: 'fullName email'
          }];
        }
        delete query.populate;
        return await storage.annotations.getAnnotations(query, populate);
      } catch (err) {
        throw (err);
      }
    },
  
    getAnnotation: async function (query) {
      try {
        let populate = getPopulateFields(query);
        if (!populate) {
          populate = [{
            path: 'createdBy',
            select: 'fullName email'
          }];
        }
        delete query.populate;
        return await storage.annotations.getAnnotation(query, populate);
      } catch (err) {
        throw (err);
      }
    },
  
    createAnnotation: async function (body, user) {
      try {
        body.createdBy = user;
        return await storage.annotations.createAnnotation(body)
      } catch (err) {
        throw (err);
      }
    },
  
    updateAnnotation: async function (annotationId, data) {
      try {
        return await storage.annotations.updateAnnotation({
          _id: annotationId
        }, data)
      } catch (err) {
        throw (err);
      }
    },
  
    removeAnnotation: async function (annotationId) {
      try {
        return await storage.annotations.removeAnnotation({
          _id: annotationId
        })
      } catch (err) {
        throw (err);
      }
    }
  }

function __createGanttRow(activity, parent, dependency) {
    let count = activity.wbsId.split(",").length - 1;
    let pClass = 'gtaskblue';
    if (count === 0) {
        pClass = 'ggroupblack';
    } else if (count === 1) {
        pClass = 'gtaskpurple';
    } else if (count === 2) {
        pClass = 'gtaskblue';
    }
    let pGroup = count;

    return {
        "pID": activity.wbsId,
        "pName": activity.name,
        "pStart": activity.actualStartDate,
        "pEnd": activity.actualFinishDate,
        "pPlanStart": activity.plannedStartDate,
        "pPlanEnd": activity.plannedFinishDate,
        "pClass": pClass,
        "pComp": activity.progress.actual,
        "pGroup": pGroup,
        "pParent": getParentWbs(activity.wbsId),
        "pOpen": 1,
        "pDepend": dependency
    };
}

function getParentWbs(wbsId) {
    let split = wbsId.lastIndexOf('.');
    if (split > -1) {
       return wbsId.substring(0, split);
    } else {
        return '0';
    }
}

let entityRoles = {

    getEntityRoles: async function (query, token) {
      try {
        let options = {
          method: 'GET',
          uri: process.env.ROLES_SERVER + '/entity-roles?' + query,
          strictSSL: false,
          headers: {
            'x-auth-token': token
          },
          json: true // Automatically stringifies the body to JSON
        };
        return await rp(options);
      } catch (err) {
        throw (err);
      }
    },
  
    createEntityRole: async function (data, token) {
      try {
        let options = {
          method: 'POST',
          uri: process.env.ROLES_SERVER + '/entity-roles',
          strictSSL: false,
          body: data,
          headers: {
            'x-auth-token': token
          },
          json: true // Automatically stringifies the body to JSON
        };
        return await rp(options);
      } catch (err) {
        throw({
          code: err.statusCode,
          message: err.error.message
        });
      }
    },
  
    deleteEntityUser: async function (userId, entities, entityType, token) {
      try {
        let query = `?user=${userId}&entityType=${entityType}`;
        let options = {
          method: 'DELETE',
          uri: process.env.ROLES_SERVER + '/entity-roles' + query,
          strictSSL: false,
          body: { entities: entities },
          headers: {
            'x-auth-token': token
          },
          json: true // Automatically stringifies the body to JSON
        };
        return await rp(options);
      } catch (err) {
        throw (err);
      }
    },
  
    updateEntityUser: async function (userId, entities, entityType, roleId, token) {
      try {
        let query = `?user=${userId}&entityType=${entityType}`;
        let options = {
          method: 'PUT',
          uri: process.env.ROLES_SERVER + '/entity-roles' + query,
          strictSSL: false,
          body: {
            role: roleId,
            entities: entities
          },
          headers: {
            'x-auth-token': token
          },
          json: true // Automatically stringifies the body to JSON
        };
        return await rp(options);
      } catch (err) {
        throw (err);
      }
    }
}

let invitations = {

    createInvitation: async function (data, token) {
        try {
            let options = {
                method: 'POST',
                uri: process.env.USERS_SERVER + '/invitations',
                strictSSL: false,
                body: data,
                headers: {
                    'x-auth-token': token
                },
                json: true // Automatically stringifies the body to JSON
            };
            return await rp(options);
        } catch (err) {
            throw (err);
        }
    },

    getInvitation: async function (invitationCode, token) {
        try {
            let options = {
                method: 'GET',
                uri: process.env.USERS_SERVER + '/invitations/' + invitationCode,
                strictSSL: false,
                headers: {
                    'x-auth-token': token
                },
                json: true // Automatically stringifies the body to JSON
            };
            return await rp(options);
        } catch (err) {
            throw (err);
        }
    },

    getInvitationByEmail: async function (email, entityId, entityType, token) {
        try {
            let query = `?to=${email}&entity=${entityId}&entityType=${entityType}`;
            let options = {
                method: 'GET',
                uri: process.env.USERS_SERVER + '/invitations/byEmail' + query,
                strictSSL: false,
                headers: {
                    'x-auth-token': token
                },
                json: true // Automatically stringifies the body to JSON
            };
            return await rp(options);
        } catch (err) {
            throw (err);
        }
    },

    removeInvitation: async function (invitationCode, token) {
        try {
            let options = {
                method: 'DELETE',
                uri: process.env.USERS_SERVER + '/invitations/' + invitationCode,
                strictSSL: false,
                headers: {
                    'x-auth-token': token
                },
                json: true // Automatically stringifies the body to JSON
            };
            return await rp(options);
        } catch (err) {
            throw (err);
        }
    }
}

function getPopulateFields(query) {
    if (query.populate) {
        let populate = [];
        let fields = query.populate.split('|');
        if (fields && fields.length > 0) {
            for (let i = 0; i < fields.length; i++) {
                populate.push({
                    path: fields[i],
                    select: '-__v -password'
                });
            }
            return populate;
        }
        return null;
    }
    return null;
}

async function sendEmail(data) {
    let options = {
        method: 'POST',
        uri: process.env.NOTIFICATION_SERVER + '/email/send',
        strictSSL: false,
        body: data,
        json: true // Automatically stringifies the body to JSON
    };
    return await rp(options);
}

async function registerUser(baseUrl, data) {
    let options = {
        method: 'POST',
        uri: process.env.USERS_SERVER + '/users/register',
        strictSSL: false,
        headers: {
            'origin': baseUrl
        },
        body: data,
        json: true // Automatically stringifies the body to JSON
    };
    return await rp(options);
}

async function getUser(email, token) {
    let options = {
        method: 'GET',
        uri: process.env.USERS_SERVER + '/users/' + email,
        strictSSL: false,
        headers: {
            'x-auth-token': token
        },
        json: true // Automatically stringifies the body to JSON
    };
    return await rp(options);
}

async function getUserProfile(token) {
    let options = {
        method: 'GET',
        uri: process.env.USERS_SERVER + '/users/me',
        strictSSL: false,
        headers: {
            'x-auth-token': token
        },
        json: true // Automatically stringifies the body to JSON
    };
    return await rp(options);
}

var tilesets = {

    listTilesets: async function (queryParams) {
        try {
            let query = {};
            if (queryParams.project_id) {
                query = {
                    project_id: queryParams.project_id
                };
            }
            return await storage.tilesets.listTilesets(query)
        } catch (err) {
            return throwErrorMessage(err);
        }
    },

    getTileset: async function (tileset_id) {
        try {
            return await storage.tilesets.getTileset({
                _id: tileset_id
            })
        } catch (err) {
            return throwErrorMessage(err);
        }
    },

    createTileset: async function (body, user) {
        try {
            body.createdBy = user;
            let tileset = await storage.tilesets.createTileset(body);
            await storage.projects.updateProject({
                _id: body.project
            }, { $addToSet: { tilesets: tileset._id } });
            var bucketParams = {
                Bucket: process.env.S3_CONSTRUCT_BUCKET,
                ACL: 'public-read',
                Key: 'projects/' + body.project + '/tilesets/' + tileset._id + '/',
                Body: ''
            };

            await awsUtils.s3_upload(bucketParams);

            return tileset;
        } catch (err) {
            return throwErrorMessage(err);
        }
    },

    updateTileset: async function (tileset_id, data) {
        try {
            return await storage.tilesets.updateTileset({
                _id: tileset_id
            }, data)
        } catch (err) {
            return throwErrorMessage(err);
        }
    },

    removeTileset: async function (tileset_id) {
        try {
            let tileset = await storage.tilesets.removeTileset({
                _id: tileset_id
            });
            await storage.projects.updateProject({
                _id: project_id
            }, {
                    $pull: {
                        tilesets: { $in: [tileset_id] }
                    }
                });
            return tileset;
        } catch (err) {
            return throwErrorMessage(err);
        }
    }
}



module.exports = {
    organisations: organisations,
    projects: projects,
    activities: activities,
    annotations: annotations,
    tilesets: tilesets
}