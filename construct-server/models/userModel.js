'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserSchema = new Schema({

    fullName: { // full name of the user
        type: String,
        trim: true
    },

    email: { // email of the user
        type: String,
        lowercase: true,
        unique: true,
        trim: true,
        required: true
    },

    password: { // password hashed using bcryptjs
        type: String,
        required: true
    },

    mobile: { // mobile number of the user.
        type: Number,
        required: false
    },

    countryCode: { // mobile country code
        type: String,
        required: false
    },

    isActive: {   //  whether user is active
        type: Boolean,
        default: true
    },

    avatar: {
        type: String, // user for user profile image
        required: false
    },

    verified: {
        email: {
            type: Boolean,
            default: false,
        },
        mobile: {
            type: Boolean,
            default: false
        }
    }

}, { timestamps: true });

module.exports = mongoose.model('User', UserSchema);
