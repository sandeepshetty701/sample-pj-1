'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var UserTokenSchema = new Schema({

    user: { //  object Id of the user for whom the token is generated
        type: Schema.Types.ObjectId,
        ref: 'User',
        required: true,
        unique: true
    },

    token: { //  the generated token
        type: String,
        trim: true,
        required: true,
        unique: true
    }

}, { timestamps: true });

/**
 *   token (document) will be deleted after expiry time
 */
//UserTokenSchema.index({ updatedAt: 1 }, { expireAfterSeconds: 3600 });

module.exports = mongoose.model('UserToken', UserTokenSchema);