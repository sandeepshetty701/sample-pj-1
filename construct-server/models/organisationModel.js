'use strict';

var mongoose = require('mongoose'),
    Schema = mongoose.Schema;

var OrganisationSchema = new Schema({

    name: { // name of the organisation
        type: String,
        trim: true,
        required: true,
        unique: true
    },

    email: { // email of the organisation
        type: String,
        lowercase: true,
        trim: true,
        required: false
    },

    mobile: { 
    /** mobile number of the contact person of organisation.
         min and max as per indian mobile numbers
    */
        type: Number,
        min: 1000000000,
        max: 9999999999,
        required: false,
    },

    countryCode: { // mobile country code
        type: String,
        default: '+91'
    },

    landlineNumber: { // landline number
        type: String
    },

    isActive: {   //  whether organisation is active
        type: Boolean,
        default: true
    },

    properties: Object

}, { timestamps: true });

 
module.exports = mongoose.model('Organisation', OrganisationSchema);