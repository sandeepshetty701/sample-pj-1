'use strict';

var mongoose = require('mongoose'),
  Schema = mongoose.Schema;

var ActivitySchema = new Schema({

  name: { //  name of the activity

    type: String,
    trim: true,
    required: true
  },

  description: { //  few lines description about the activity

    type: String,
    trim: true
  },

  createdBy: { //  object Id of user who created this activity

    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  owner: { //  object Id of user who is the owner of this activity

    type: Schema.Types.ObjectId,
    ref: 'User',
    required: true
  },

  isCritical: {

    type: Boolean,
    trim: true,
    default: false
  },

  project: { //  object Id of project under which the activity is created

    type: Schema.Types.ObjectId,
    ref: 'Project',
    required: true
  },

  wbsId: { // WBS ID of the activity

    type: String,
    trim: true,
    required: true
  },

  builder: { // constructor for this activity

    type: String,
    trim: true
  },

  resources: [String], //     all the resources for this activity

  tags: [String], //     all the tags for this activity

  weightage: {  //  weightage of this activity

    type: Number,
    default: 1
  },

  scope: {
    quantity: {
      type: Number,
      default: 100
    },
    units: {
      type: String,
      default: '%'
    }
  },

  snapshot: [{  //  quantity (progress) date-wise

    date: Date,
    actual: Number,
    planned: Number
  }],

  duration: { // duration in days

    type: Number,
    default: -1
  },

  plannedStartDate: Date, // Equals to the Min Start Date of the sub-activitys (Planned)

  plannedFinishDate: Date, // Equals to the Max End Date of the sub-activitys (Planned)

  plannedCost: Number, // planned cost of the project in INR.

  actualStartDate: Date, // Equals to the Min Start Date of the sub-activitys (Actual). Forecasted for future dates

  actualFinishDate: Date, // Equals to the Max End Date of the sub-activitys (Actual). Forecasted for future dates

  actualCost: Number, // actual cost (till date) of the project in INR.

  manpower: [{ // manpower (count) needed for this activity (for each date)

    date: Date,
    data: [{

      type: String, // type of manpower. Ex: Rebar, Carpenter etc...
      planned: Number,
      actual: Number
    }]
  }],

  offsetDays: Number, //  Lag/Lead for the dependency (+ve for Lag and -ve for Lead)

  totalFloat: Number, // Calculated on the fly

  freeFloat: Number, // Calculated on the fly

  predecessors: [{

    wbsId: { // WBS Id of the activity.

      type: String
      // ref: 'Activity'
    },
    relation: { //  Can be one of FF, SS, FS, SF

      type: String,
      default: 'FS'
    },
    offset: {

      type: Number,
      default: 0
    }
  }],

  status: String, // Status of the project (Ex: On Track, Delayed, In Advance)

  properties: Object // properties describing the annotation

},
  {
    timestamps: true,
    toObject: { virtuals: true },
    toJSON: { virtuals: true }
  });

ActivitySchema.index({ wbsId: 1, project: 1 }, { unique: true })

ActivitySchema.virtual('progress').get(function () {
  try {
    if (this.snapshot && this.snapshot.length > 0) {
      let today = new Date();
      let actual_index = -1;
      let planned_index = -1;
      for (let i = this.snapshot.length - 1; i > -1; i--) {
        if (this.snapshot[i].actual !== undefined && this.snapshot[i].date <= today) {
          actual_index = i;
          break;
        }
      }
      for (let i = this.snapshot.length - 1; i > -1; i--) {
        if (this.snapshot[i].planned !== undefined && this.snapshot[i].date <= today) {
          planned_index = i;
          break;
        }
      }
      if (this.snapshot.length > actual_index && actual_index > -1 && this.snapshot.length > planned_index && planned_index > -1) {
        let mProgress = {
          date: '',
          actual: 0,
          planned: 0,
          actual_progress: 0,
          planned_progress: 0
        }
        let actual_progress = this.snapshot[actual_index];
        let planned_progress = this.snapshot[planned_index];
        mProgress.date = actual_progress.date;
        mProgress.actual = actual_progress.actual;
        mProgress.planned = planned_progress.planned;
        if (actual_progress.actual) {
          mProgress.actual_progress = this.scope.quantity == 0 ? 100 : +((actual_progress.actual * 100 / this.scope.quantity).toFixed(2));
        }
        mProgress.planned_progress = this.scope.quantity == 0 ? 100 :  +(planned_progress.planned * 100 / this.scope.quantity).toFixed(2);
        return mProgress;
      } else {
        return {
          planned: 0,
          actual: 0
        };
      }
    } else {
      return {
        date: new Date(),
        planned: 0,
        actual: 0
      };
    }
  } catch (err) {
    console.log(err);
    return {
      planned: 0,
      actual: 0
    };
  }
});

module.exports = mongoose.model('Activity', ActivitySchema);
