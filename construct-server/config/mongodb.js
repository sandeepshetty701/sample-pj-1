const mongoose = require('mongoose');

require('dotenv').config();

mongoose.Promise = global.Promise;
mongoose.connect(process.env.MONGODB_URI, { useNewUrlParser: true, useUnifiedTopology: true, useFindAndModify: false });

require('../models/organisationModel');
require('../models/userTokenModel');
require('../models/activityModel');
require('../models/projectModel');
require('../models/tilesetModel');
require('../models/userModel');
require('../models/annotationModel');

module.exports = {
    models: {
        Activity: mongoose.model('Activity'),
        Organisation: mongoose.model('Organisation'),
        Project: mongoose.model('Project'),
        Tileset: mongoose.model('Tileset'),
        User: mongoose.model('User'),
        Annotation: mongoose.model('Annotation'),
        UserToken: mongoose.model('UserToken')
    }
}