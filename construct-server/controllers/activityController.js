'use strict';

require('../models/projectModel');
require('../models/activityModel');


var storage = require('../lib/storage');
const blogic = require('../lib/blogic');
var logger = require('../utils/logger').getLogger('Project Controller');
var ObjectId = require('mongoose').Types.ObjectId;

module.exports = {

  get_activities: function (req, res) {
    get_activities(req, res);
  },

  get_activity_hierarchy: function (req, res) {
    get_activity_hierarchy(req, res);
  },

  get_activity_gantt: function (req, res) {
    get_activity_gantt(req, res);
  },

  get_activity_by_id: function (req, res) {
    get_activity_by_id(req, res);
  },

  create_new_activity: function (req, res) {
    create_new_activity(req, res);
  },

  update_activity: function (req, res) {
    update_activity(req, res);
  },

  update_activity_progress: function (req, res) {
    update_activity_progress(req, res);
  },

  delete_activity: function (req, res) {
    delete_activity(req, res);
  }

}

async function processErrorMessage(err, res) {
  let resObj = {
    success: false,
    message: err.message
  };
  return res.status(err.code ? err.code : 500).json(resObj);
}

/**
 *   retrieve the activities associated to the project
 */

var get_activities = async function (req, res) {
  try {
    let query = req.query;
    if (!query) {
      query = {};
    }
    query.project = req.params.id;
    var resObj = await blogic.activities.getActivities(query);
    resObj.status = 'success';
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return processErrorMessage(err, res);
  }
}

/**
 *   Get the activity hierarchy
 */

var get_activity_hierarchy = async function (req, res) {
  try {
    var resObj = await blogic.activities.getActivityHierarchy(req.params.id);
    resObj.status = 'success';
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return processErrorMessage(err, res);
  }
}

/**
 *   Get the activity gantt
 */

var get_activity_gantt = async function (req, res) {
  try {
    var resObj = await blogic.activities.getGanttChart(req.params.id);
    resObj.status = 'success';
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return processErrorMessage(err, res);
  }
}

/**
 *   Get the activity info from object id
 */

var get_activity_by_id = async function (req, res) {
  try {
    let query = {
      _id: req.params.activity_id
    };
    var resObj = await storage.mongoDB.activities.getActivity(query);
    resObj.status = 'success';
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return processErrorMessage(err, res);
  }
}

/**
 *   create a new activity/sub-activity
 */

var create_new_activity = async function (req, res) {
  try {
    var resObj = await blogic.activities.create_new_activity(req.body, req.params.id, req.decoded.user._id);
    logger.info("Request fullfilled - 201 CREATED");
    return res.status(201).json(resObj);
  } catch (err) {
    return processErrorMessage(err, res);
  }
}

/**
 *   update the activity info based on the id passed as path param
 */

var update_activity = async function (req, res) {
  try {
    let query = {
      _id: req.params.activity_id
    }
    var resObj = await blogic.activities.updateActivity(query, req.body);
    resObj.status = 'success';
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return processErrorMessage(err, res);
  }
}

/**
 *   update the activity progress (snapshot) based on the date provided in query param
 */

var update_activity_progress = async function (req, res) {
  try {
    let date = new Date(req.query.date);
    let query = {
      _id: req.params.activity_id,
      'snapshot.date': date
    };
    var resObj = await blogic.activities.updateActivityProgress(query, req.body, date);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return processErrorMessage(err, res);
  }
}

/**
 *   delete the activity based on the id passed as path param
 */

var delete_activity = async function (req, res) {
  try {
    let query = {
      _id: req.params.activity_id
    }
    var resObj = await blogic.activities.deleteActivity(query);
    logger.info("Request fullfilled - 200 OK");
    return res.status(204).json(resObj);
  } catch (err) {
    return processErrorMessage(err, res);
  }
}