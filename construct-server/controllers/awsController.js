'use strict';

require('dotenv').config();

var AWS = require('aws-sdk');
var rp = require('request-promise');
var logger = require('../utils/logger').getLogger('AWS Controller');

async function __getS3Instance() {
  //let credentials = await __getEC2Credentials();
  let credentials = await __getStaticCredentials();
  if (credentials) {
    AWS.config.update({
      accessKeyId: credentials.AccessKeyId,
      secretAccessKey: credentials.SecretAccessKey,
      sessionToken: credentials.Token
    });
    return new AWS.S3({ signatureVersion: 'v4' });
  }
}

async function __getStaticCredentials() {
  return {
    AccessKeyId: 'AKIAJHZ3KUFHZDLW4U2A',
    SecretAccessKey: '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'
  }
}

async function __getEC2Credentials() {
  try {
    let data = await rp(process.env.AWS_IAM_METADATA + 'ec2s3rw');
    if (data != null) {
      return JSON.parse(data);
    }
  } catch (error) {
    console.log(error);
    return null;
  }
}

async function __getObjectsList(req) {
  if (!req.query.bucket) {
    return { status: 404, message: 'Bucket not found' };
  }
  if (!req.query.path) {
    return { status: 404, message: 'Path not found' };
  }
  let S3 = await __getS3Instance();
  let result = [];
  if (!req.query.path) {
    return result;
  }
  const listParams = {
    Bucket: req.query.bucket,
    Prefix: req.query.path
  };
  const listedObjects = await S3.listObjectsV2(listParams).promise();
  listedObjects.Contents.forEach(({ Key }) => {
    result.push(Key);
  });
  return result;
}

async function __deleteObject(req) {
  if (!req.query.bucket) {
    return { status: 404, message: 'Bucket not found' };
  }
  if (!req.query.path) {
    return { status: 404, message: 'Path not found' };
  }
  let S3 = await __getS3Instance();
  console.log(req.query.path)
  const deleteParams = {
    Bucket: req.query.bucket,
    Key: req.query.path
  };
  return await S3.deleteObject(deleteParams).promise();
}

async function __deleteObjects(req) {
  if (!req.query.bucket) {
    return { status: 404, message: 'Bucket not found' };
  }
  if (!req.query.path) {
    return { status: 404, message: 'Path not found' };
  }
  const listParams = {
    Bucket: req.query.bucket,
    Prefix: req.query.path + '/'
  };
  let S3 = await __getS3Instance();
  const listedObjects = await S3.listObjectsV2(listParams).promise();

  if (listedObjects.Contents.length === 0) return;

  let deleteParams = {
    Bucket: req.query.bucket,
    Delete: { Objects: [] }
  };

  let result = await S3.deleteObjects(deleteParams).promise();

  deleteParams = {
    Bucket: req.query.bucket,
    Key: req.query.path
  };
  result = await S3.deleteObject(deleteParams).promise();

  if (listedObjects.IsTruncated) await emptyS3Directory(bucket, dir);
}

async function __uploadObject(req, res) {
  console.log(req)
  if (Object.keys(req.files).length == 0) {
    return res.status(400).send('No files were uploaded.');
  }

  // The name of the input field (i.e. "sampleFile") is used to retrieve the uploaded file
  let sampleFile = req.files.sampleFile;
  console.log(sampleFile)
}

async function __uploadFiles(req, res) {
  if (Object.keys(req.files).length == 0) {
    return res.status(400).send('No files were uploaded.');
  }

  let S3 = await __getS3Instance();
  const file = req.files;
  const path = req.query.path ? req.query.path + '/' : '';
  let ResponseData = [];
  file.map((item) => {
    var params = {
      Bucket: req.query.bucket,
      Key: path + item.originalname,
      Body: item.buffer,
      ACL: 'public-read'
    };
    S3.upload(params, function (err, data) {
      if (err) {
        
      } else {
        ResponseData.push(data);
        if (ResponseData.length == file.length) {
          res.json({ success: true, result: ResponseData });
        }
      }
    });
  });
}

async function __uploadSingleFile(bucket, path, file) {
  let S3 = await __getS3Instance();
  let ResponseData = [];
  var params = {
    Bucket: bucket,
    Key: path,
    Body: file.buffer,
    ACL: 'public-read'
  };
  S3.upload(params, function (err, data) {
    if (err) {
      res.status(500).json({ success: false, message: 'Upload Failed'});
    } else {
      ResponseData.push(data);
      if (ResponseData.length == file.length) {
        res.json({ success: true, result: ResponseData });
      }
    }
  });
}

async function __putObject(req, res) {
  let S3 = await __getS3Instance();
  var bucketParams = {
    Bucket: req.query.bucket,
    ACL: 'public-read',
    Key: req.query.path,
    Body: JSON.stringify(req.body)
  };
  try {
    let s3Put = await S3.putObject(bucketParams).promise();
    return s3Put;
  } catch (err) {
    console.log(err);
    return null;
  }
}

exports.getObjectsList = function (req, res) {
  __getObjectsList(req)
    .then((result) => {
      if (result.status) {
        res.status(result.status).json(result)
      } else {
        res.send(result);
      }
    })
    .catch((err) => {
      res.send(err);
    });
}

exports.deleteObject = function (req, res) {
  __deleteObject(req)
    .then((result) => {
      console.log(result)
      if (result.status) {
        res.status(result.status).json(result)
      } else {
        res.status(204).send();
      }
    })
    .catch((err) => {
      res.send(err);
    });
}

exports.deleteObjects = function (req, res) {
  __deleteObjects(req)
    .then((result) => {
      if (result.status) {
        res.status(result.status).json(result)
      } else {
        res.status(204).send();
      }
    })
    .catch((err) => {
      res.send(err);
    });
}

exports.uploadObject = function (req, res) {
  __uploadObject(req)
    .then((result) => {
      if (result.status) {
        res.status(result.status).json(result)
      } else {
        res.status(201).send(result);
      }
    })
    .catch((err) => {
      res.send(err);
    });
}

exports.putObject = function (req, res) {
  __putObject(req)
    .then((result) => {
      if (result.status) {
        res.status(result.status).json(result)
      } else {
        res.status(201).send(result);
      }
    })
    .catch((err) => {
      res.send(err);
    });
}

var s3Upload = async function (req, res) {
  try {
    let S3 = await __getS3Instance();
    var resObj = await S3.upload(req.body).promise();
    resObj.status = 'success';
    logger.info("Request fullfilled - 201 OK");
    return res.status(201).json(resObj);
  } catch (err) {
    let resObj = {
      success: false,
      message: "Unable to upload to s3"
    };
    return res.status(500).json(resObj);
  }


}

module.exports = {
  getS3Instance: __getS3Instance,
  s3Upload: s3Upload,
  uploadFiles: __uploadFiles,
  uploadSingleFile: __uploadSingleFile
}
