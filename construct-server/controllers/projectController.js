'use strict';

var blogic = require('../lib/blogic'),
  storage = require('../lib/storage'),
  logger = require('../utils/logger').getLogger('Project Controller'),
  Utils = require('../utils/projects');
 
module.exports = {

  listMyProjectsFlat: function (req, res) {
    listMyProjectsFlat(req, res)
  },

  createProject: function (req, res) {
    createProject(req, res)
  },

  getProject: function (req, res) {
    getProject(req, res)
  },

  getProjectPlan: function (req, res) {
    getProjectPlan(req, res)
  },

  getProjectGantt: function (req, res) {
    getProjectGantt(req, res)
  },

  downloadProjectPlan: function (req, res) {
    downloadProjectPlan(req, res)
  },

  uploadProjectPlan: function (req, res) {
    uploadProjectPlan(req, res)
  },

  updateProject: function (req, res) {
    updateProject(req, res)
  },

  deleteProject: function (req, res) {
    deleteProject(req, res)
  },

  assignUser: function (req, res) {
    assignUser(req, res)
  },

  deassignUser: function (req, res) {
    deassignUser(req, res)
  },

  makeProjectAdmin: function (req, res) {
    makeProjectAdmin(req, res)
  },

  removeProjectAdmin: function (req, res) {
    removeProjectAdmin(req, res)
  },

  getProjectBreadcrumb: function (req, res) {
    getProjectBreadcrumb(req, res)
  },

  getProjectBreadcrumbArray: function (req, res) {
    getProjectBreadcrumbArray(req, res)
  },

  getProjectHierarchy: function (req, res) {
    getProjectHierarchy(req, res)
  },

  incUnreadCount: function (req, res) {
    incUnreadCount(req, res)
  },

  resetUnreadCount: function (req, res) {
    resetUnreadCount(req, res)
  },

  changeRole: function (req, res) {
    changeRole(req, res)
  }
}

var listMyProjectsFlat = async function (req, res) {
  try {
    let user = req.decoded;
    var resObj = await blogic.projects.listMyProjectsFlat(user);
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res)
  }
}

var getProject = async function (req, res) {
  try {
    var resObj = await blogic.projects.getProject(req.params.id, req.query);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var getProjectPlan = async function (req, res) {
  try {
    var resObj = await blogic.projects.getProjectPlan(req.params.id);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var getProjectGantt = async function (req, res) {
  try {
    var resObj = await blogic.projects.getProjectGantt(req.params.id);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var downloadProjectPlan = async function (req, res) {
  try {
    var resObj = await blogic.projects.downloadProjectPlan(req.params.id);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var uploadProjectPlan = async function (req, res) {
  try {
    var resObj = await blogic.projects.uploadProjectPlan(req.decoded.user, req.params.id, req.file);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var createProject = async function (req, res) {
  try {
    var resObj = await blogic.projects.createProject(req.decoded, req.body);
    logger.info("Request fullfilled - 201 OK");
    return res.status(201).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var updateProject = async function (req, res) {
  try {
    let query = { _id: req.params.id };
    if (req.body.name) {
      req.body.slug = req.body.name.replace(/ +/g, '');
    }
    var resObj = await blogic.projects.updateProject(query, req.body);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var deleteProject = async function (req, res) {
  try {
    let resObj = {}
    await blogic.projects.deleteProject(req.params.id);
    logger.info("Request fullfilled - 204 OK");
    return res.status(204).json();
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var assignUser = async function (req, res) {
  try {
    let base_url = req.get('origin');
        var resObj = await blogic.projects.inviteUser(base_url, req.decoded, 
            req.params.id, req.body.email, req.body.role);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
};

var deassignUser = async function (req, res) {
  try {
    let resObj = {};
    resObj = await blogic.projects.deassignUser(req.params.id, req.body.user, req.decoded.token);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
};

var changeRole = async function (req, res) {
  try {
    let resObj = await blogic.projects.changeRole(req.params.id, req.body.user, req.body.role, req.decoded.token);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
};

var makeProjectAdmin = async function (req, res) {
  try {
    let resObj = await blogic.projects.makeProjectAdmin(req.body.user, req.params.id);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
};

var removeProjectAdmin = async function (req, res) {
  try {
    let resObj = await blogic.projects.removeProjectAdmin(req.body.user, req.params.id);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var getProjectBreadcrumb = async function (req, res) {
  try {
    let resObj = await blogic.projects.getProjectBreadcrumb(req.params.id);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    console.log(err)
    return await processErrorMessage(err, res);
  }
}

var getProjectBreadcrumbArray = async function (req, res) {
  try {
    let user = req.decoded.user;
    let resObj = await blogic.projects.getProjectBreadcrumbArray(user, req.params.id);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    console.log(err)
    return await processErrorMessage(err, res);
  }
}

var getProjectHierarchy = async function (req, res) {
  try {
    let resObj = await blogic.projects.getProjectHierarchy(req.query.organisation);
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    console.log(err)
    return await processErrorMessage(err, res);
  }
}

var incUnreadCount = async function (req, res) {
  try {
    let resObj = await blogic.projects.incUnreadCount(req.params.id);
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

var resetUnreadCount = async function (req, res) {
  try {
    let resObj = await blogic.projects.resetUnreadCount(req.params.id,req.decoded.user);
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

/**
 *   HANDLE ERROR MESSAGES
 */
async function processErrorMessage(err, res) {
  let resObj = {
    success: false,
    message: err.message
  };
  return res.status(err.code ? err.code : 500).json(resObj);
}



