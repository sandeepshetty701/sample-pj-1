'use strict';

const blogic = require('../lib/blogic');

let logger = require("../utils/logger").getLogger("ORGANISATION");
/**
 * 
 * @param {*} req 
 * @param {*} res 
 */

module.exports = {

    createOrganisation: function (req, res) {
        createOrganisation(req, res)
    },

    getOrganisations: function (req, res) {
        getOrganisations(req, res)
    },

    getMyOrganisations: function (req, res) {
        getMyOrganisations(req, res)
    },

    getOrganisation: function (req, res) {
        getOrganisation(req, res)
    },

    uploadLogo: function (req, res) {
        uploadLogo(req, res)
    },

    assignUser: function (req, res) {
        assignUser(req, res)
    },

    deassignUser: function (req, res) {
        deassignUser(req, res)
    },

    changeRole: function (req, res) {
        changeRole(req, res)
    },

    updateOrganisation: function (req, res) {
        updateOrganisation(req, res)
    },

    deleteOrganisation: function (req, res) {
        deleteOrganisation(req, res)
    }
}

let createOrganisation = async function (req, res) {
    try {
        let base_url = req.get('origin');
        var resObj = await blogic.organisations.createOrganisation(base_url, req.body);
        logger.info("Request fullfilled - 201 CREATED");
        return res.status(201).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let getOrganisations = async function (req, res) {
    try {
        var resObj = await blogic.organisations.getOrganisations(req.query);
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let getMyOrganisations = async function (req, res) {
    try {
        var resObj = await blogic.organisations.getMyOrganisations(req.decoded, req.query);
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let getOrganisation = async function (req, res) {
    try {
        var resObj = await blogic.organisations.getOrganisation({
            _id: req.params.orgId
        });
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let uploadLogo = async function (req, res) {
    try {
        var resObj = await blogic.organisations.uploadLogo(req.params.orgId, req.file);
        logger.info("Request fullfilled - 200 OK");
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let assignUser = async function (req, res) {
    try {
        let base_url = req.get('origin');
        var resObj = await blogic.organisations.inviteUser(base_url, req.decoded, 
            req.params.orgId, req.body.email, req.body.role);
        return res.status(201).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let deassignUser = async function (req, res) {
    try {
        var resObj = await blogic.organisations.deassignUser(req.params.orgId, req.query.user, req.decoded.token);
        return res.status(204).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let changeRole = async function (req, res) {
    try {
        var resObj = await blogic.organisations.changeRole(req.params.orgId, 
            req.body.user, req.body.role, req.decoded.token);
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let updateOrganisation = async function (req, res) {
    try {
        var resObj = await blogic.organisations.updateOrganisation(req.query, req.body);
        return res.status(200).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

let deleteOrganisation = async function (req, res) {
    try {
        var resObj = await blogic.organisations.deleteOrganisation(req.query);
        return res.status(204).json(resObj);
    } catch (err) {
        return await processErrorMessage(err, res)
    }
}

/**
 *   HANDLE ERROR MESSAGES
 */
async function processErrorMessage(err, res) {
    let resObj = {
        success: false,
        message: err.message
    };
    if(err.message.indexOf(' - ') > -1) {
        err.code = +(err.message.split(' - ')[0]);
        resObj = JSON.parse(err.message.split(' - ')[1]);
    }
    return res.status(err.code ? err.code : 500).json(resObj);
}
