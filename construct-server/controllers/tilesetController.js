'use strict';

var blogic = require('../lib/blogic'),
  logger = require('../utils/logger').getLogger('Tileset Controller');

module.exports = {

  list_tilesets: function (req, res) {
    list_tilesets(req, res)
  },

  get_tileset_by_id: function (req, res) {
    get_tileset_by_id(req, res)
  },

  create_tileset: function (req, res) {
    create_tileset(req, res)
  },

  update_tileset: function (req, res) {
    update_tileset(req, res)
  },

  delete_tileset: function (req, res) {
    delete_tileset(req, res)
  }
}

/**
 *   retrieve the tilesets associated to the project
 */

var list_tilesets = async function (req, res) {
  try {
    let resObj = await blogic.tilesets.listTilesets(req.query);
    resObj.status = 'success';
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

/**
 *   Get the tileset info from object id
 */
var get_tileset_by_id = async function (req, res) {
  try {
    let resObj = await blogic.tilesets.getTileset(req.params.tid);
    resObj.status = 'success';
    logger.info("Request fullfilled - 200 OK");
    return res.status(200).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
}

/**
 *   create a new tileset and assign to an project
 */
var create_tileset = async function (req, res) {
  try {
    let resObj = await blogic.tilesets.createTileset(req.body, req.decoded.user);
    resObj.status = 'success';
    logger.info("Request fullfilled - 201 CREATED");
    return res.status(201).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
};

/**
 *   update tileset
 */
var update_tileset = async function (req, res) {
  try {
    let resObj = await blogic.tilesets.updateTileset(req.params.tid, req.body);
    resObj.status = 'success';
    logger.info("Request fullfilled - 201 OK");
    return res.status(201).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
};

/**
 *   delete tileset
 */
var delete_tileset = async function (req, res) {
  try {
    await blogic.tilesets.removeTileset(req.params.tid);
    let resObj = {};
    resObj.status = 'success';
    logger.info("Request fullfilled - 204 DELETED");
    return res.status(204).json(resObj);
  } catch (err) {
    return await processErrorMessage(err, res);
  }
};

/**
 *   HANDLE ERROR MESSAGES
 */
async function processErrorMessage(err, res) {
  if (err.statusCode) {
    return res.status(err.statusCode).json(err);
  } else {
   let resObj = {
      status: 'Error',
      statusCode: 500,
      message: 'Internal Server Error',
      description: err
    };
    logger.error(resObj);
    return res.status(500).json(resObj);
  }
}
