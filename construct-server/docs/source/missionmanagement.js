
/**
 *
 * @api {get} /api/mission-management/workflow/:projectId/:type  lists the workflow details for the given project id and type.
 * @apiName list_workflow
 * @apiGroup Workflow
 * @apiVersion  1.0.0
*
*
* @apiParam  {uuid} :projectId Project's id 
* @apiParam  {String} :type type
*
* @apiSuccess (Success 200) {Object[]} workflow workflow details
*
* @apiParamExample  {Object} Request-Example:
* http://52.66.177.173:8001/api/mission-management/workflow/5abfe1168c0d217615448d16/DRONE
* 
* @apiSuccessExample Success-Response:
[
    {
        "steps": [
            {
                "name": "FLIGHT"
            },
            {
                "name": "UPLOAD"
            },
            {
                "name": "IMAGE_PROCESS",
                "child_steps": [
                    {
                        "name": "ODM_PROCESS"
                    },
                    {
                        "name": "PC_POST",
                        "child_steps": [
                            {
                                "name": "LASZIP"
                            },
                            {
                                "name": "SNIP_PC"
                            },
                            {
                                "name": "POTREE_CONV"
                            }
                        ]
                    },
                    {
                        "name": "ORTHO_POST",
                        "child_steps": [
                            {
                                "name": "SNIP_ORTHO"
                            },
                            {
                                "name": "ORTHO_TILES"
                            }
                        ]
                    },
                    {
                        "name": "CAMERAS"
                    },
                    {
                        "name": "THUMBNAILS"
                    },
                    {
                        "name": "UPLOAD_S3"
                    }
                ]
            }
        ],
        "_id": "5c6aa8eed1aa900af7171ea8",
        "type": "DRONE_DEFAULT",
        "createdAt": "2019-02-18T12:45:34.980Z",
        "updatedAt": "2019-02-18T12:45:34.980Z",
        "__v": 0
    }
]
* @apiErrorExample {json}  Server error
*    HTTP/1.1 500 Internal Server Error
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} DB_Error 
*    HTTP/1.1 500 No entry in database
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} ParamError
*    HTTP/1.1 400 
*    {
*       success: false,
*       status: 'Error'
*    }
*/


/**
 *
 * @api {get} /api/mission-management/mission/:missionId  read the mission
 * @apiName list_mission
 * @apiGroup Missions
 * @apiVersion  1.0.0
*
*
* @apiParam  {uuid} :missionId mission's id 
*
* @apiSuccess (Success 200) {Object} missions read mission
*
* @apiParamExample  {Object} Request-Example:
* http://52.66.177.173:8001/api/mission-management/mission/5c6d47da6ed6254f6a21f40d
*
* @apiSuccessExample Success-Response:
*{
    "steps": [
        {
            "name": "FLIGHT"
        },
        {
            "name": "UPLOAD"
        },
        {
            "name": "IMAGE_PROCESS",
            "child_steps": [
                {
                    "name": "ODM_PROCESS"
                },
                {
                    "name": "PC_POST",
                    "child_steps": [
                        {
                            "name": "LASZIP"
                        },
                        {
                            "name": "SNIP_PC"
                        },
                        {
                            "name": "POTREE_CONV"
                        }
                    ]
                },
                {
                    "name": "ORTHO_POST",
                    "child_steps": [
                        {
                            "name": "SNIP_ORTHO"
                        },
                        {
                            "name": "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name": "CAMERAS"
                },
                {
                    "name": "THUMBNAILS"
                },
                {
                    "name": "UPLOAD_S3"
                }
            ]
        }
    ],
    "_id": "5c6d47da6ed6254f6a21f40d",
    "project": "5abf81b88c0d217615448d01",
    "type": "DRONE",
    "mission_data": {
        "name": "mission1"
    },
    "name": "side mission",
    "description": "testing",
    "createdAt": "2019-02-20T12:28:10.887Z",
    "updatedAt": "2019-02-20T12:28:10.887Z",
    "__v": 0
}
* @apiErrorExample {json}  Server error
*    HTTP/1.1 500 Internal Server Error
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} DB_Error
*    HTTP/1.1 500 No entry in database
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} NoMissionEntry
*    HTTP/1.1 400 Invalid mission id given
*    {
*       success: false,
*       status: 'Error'
*    }
*/


 /**
*
* @api {get} /api/mission-management/mission  search missions
* @apiName search missions by type or project
* @apiGroup Missions
* @apiVersion  1.0.0
*
*
* @apiParam  {String} type type
* @apiParam  {uuid} project project's id   
* @apiSuccess (Success 200) {Object[]} mission searched missions 
*
* @apiParamExample  {Object} Request-Example:
* http://52.66.177.173:8001/api/mission-management/mission/?project=5abf81b88c0d217615448d01&type=DRONE
* http://52.66.177.173:8001/api/mission-management/mission/?type=DRONE
* http://52.66.177.173:8001/api/mission-management/mission/
* @apiSuccessExample Success-Response:
*
   [
    {
        "steps": [
            {
                "name": "FLIGHT"
            },
            {
                "name": "UPLOAD"
            },
            {
                "name": "IMAGE_PROCESS",
                "child_steps": [
                    {
                        "name": "ODM_PROCESS"
                    },
                    {
                        "name": "PC_POST",
                        "child_steps": [
                            {
                                "name": "LASZIP"
                            },
                            {
                                "name": "SNIP_PC"
                            },
                            {
                                "name": "POTREE_CONV"
                            }
                        ]
                    },
                    {
                        "name": "ORTHO_POST",
                        "child_steps": [
                            {
                                "name": "SNIP_ORTHO"
                            },
                            {
                                "name": "ORTHO_TILES"
                            }
                        ]
                    },
                    {
                        "name": "CAMERAS"
                    },
                    {
                        "name": "THUMBNAILS"
                    },
                    {
                        "name": "UPLOAD_S3"
                    }
                ]
            }
        ],
        "_id": "5c6aaf9463186b0ef9a549bd",
        "project": "5abf81b88c0d217615448d01",
        "type": "DRONE",
        "mission_data": "{\"name\": \"mission7\"}",
        "name": "side mission",
        "description": "testing",
        "createdAt": "2019-02-18T13:13:56.797Z",
        "updatedAt": "2019-02-20T10:30:43.372Z",
        "__v": 0
    },
    {
        "steps": [
            {
                "name": "FLIGHT"
            },
            {
                "name": "UPLOAD"
            },
            {
                "name": "IMAGE_PROCESS",
                "child_steps": [
                    {
                        "name": "ODM_PROCESS"
                    },
                    {
                        "name": "PC_POST",
                        "child_steps": [
                            {
                                "name": "LASZIP"
                            },
                            {
                                "name": "SNIP_PC"
                            },
                            {
                                "name": "POTREE_CONV"
                            }
                        ]
                    },
                    {
                        "name": "ORTHO_POST",
                        "child_steps": [
                            {
                                "name": "SNIP_ORTHO"
                            },
                            {
                                "name": "ORTHO_TILES"
                            }
                        ]
                    },
                    {
                        "name": "CAMERAS"
                    },
                    {
                        "name": "THUMBNAILS"
                    },
                    {
                        "name": "UPLOAD_S3"
                    }
                ]
            }
        ],
        "_id": "9898989898186b0ef9a549bd",
        "project": "5abf81b88c0d217615448d01",
        "type": "DRONE",
        "mission_data": {
            "paths": [
                {
                    "name": "Water Tank",
                    "type": "SingleGrid",
                    "altitude": 100,
                    "side_overlap": "",
                    "front_overlap": "",
                    "camera_angle": 45,
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": []
                    }
                },
                {
                    "name": "Water Tank",
                    "type": "DoubleGrid",
                    "altitude": 100,
                    "side_overlap": "",
                    "front_overlap": "",
                    "camera_angle": 45,
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": []
                    }
                },
                {
                    "name": "Water Tank",
                    "type": "CircularGrid",
                    "altitude": 100,
                    "side_overlap": "",
                    "front_overlap": "",
                    "camera_angle": 45,
                    "geometry": {
                        "type": "Polygon",
                        "coordinates": []
                    }
                }
            ],
            "boundary": {
                "type": "Polygon",
                "coordinates": []
            }
        },
        "name": "Erode_test_mission_1",
        "description": "testing",
        "createdAt": "2019-02-20T13:13:56.797Z",
        "updatedAt": "2019-02-21T10:53:04.824Z",
        "__v": 0
    },
    {
        "steps": [
            {
                "name": "FLIGHT"
            },
            {
                "name": "UPLOAD"
            },
            {
                "name": "IMAGE_PROCESS",
                "child_steps": [
                    {
                        "name": "ODM_PROCESS"
                    },
                    {
                        "name": "PC_POST",
                        "child_steps": [
                            {
                                "name": "LASZIP"
                            },
                            {
                                "name": "SNIP_PC"
                            },
                            {
                                "name": "POTREE_CONV"
                            }
                        ]
                    },
                    {
                        "name": "ORTHO_POST",
                        "child_steps": [
                            {
                                "name": "SNIP_ORTHO"
                            },
                            {
                                "name": "ORTHO_TILES"
                            }
                        ]
                    },
                    {
                        "name": "CAMERAS"
                    },
                    {
                        "name": "THUMBNAILS"
                    },
                    {
                        "name": "UPLOAD_S3"
                    }
                ]
            }
        ],
        "_id": "5c6d47da6ed6254f6a21f40d",
        "project": "5abf81b88c0d217615448d01",
        "type": "DRONE",
        "mission_data": {
            "name": "mission1"
        },
        "name": "side mission",
        "description": "testing",
        "createdAt": "2019-02-20T12:28:10.887Z",
        "updatedAt": "2019-02-20T12:28:10.887Z",
        "__v": 0
    }
]

* @apiErrorExample {json}  Server error
*    HTTP/1.1 500 Internal Server Error
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} DB_Error
*    HTTP/1.1 500 No entry in database
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} NoMissionEntry
*    HTTP/1.1 400 Invalid mission id given
*    {
*       success: false,
*       status: 'Error'
*    }
*/


/**
 * 
 * @api {post} /api/mission-management/mission create mission
 * @apiName create_new_mission
 * @apiGroup Missions
 * @apiVersion  1.0.0
 * 
 * 
 * 
 * @apiParam  {uuid} project project's id
 * @apiParam  {String} type type
 * @apiParam  {Object} mission_data mission's data
 * @apiParam  {String} [description] mission's description
 * @apiParam  {String} name mission's name
 * 
 * @apiExample Example usage:
 *    http://52.66.177.173:8001/api/mission-management/mission
 * 
 * @apiParamExample  {json} Request-Example:
 * {
 *    "project":"5abf81b88c0d217615448d01",
 *    "type":"DRONE",
 *    "mission_data":{"name":  "mission1"},
 *    "name":"side mission",
 *    "description":"testing"
 *}
 * 
 * @apiSuccess (Success 200) {json} mission created mission
 * 
 * @apiSuccessExample {json} Success-Response:
 *{
    "steps": [
        {
            "name": "FLIGHT"
        },
        {
            "name": "UPLOAD"
        },
        {
            "name": "IMAGE_PROCESS",
            "child_steps": [
                {
                    "name": "ODM_PROCESS"
                },
                {
                    "name": "PC_POST",
                    "child_steps": [
                        {
                            "name": "LASZIP"
                        },
                        {
                            "name": "SNIP_PC"
                        },
                        {
                            "name": "POTREE_CONV"
                        }
                    ]
                },
                {
                    "name": "ORTHO_POST",
                    "child_steps": [
                        {
                            "name": "SNIP_ORTHO"
                        },
                        {
                            "name": "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name": "CAMERAS"
                },
                {
                    "name": "THUMBNAILS"
                },
                {
                    "name": "UPLOAD_S3"
                }
            ]
        }
    ],
    "_id": "5c6d2bd26ed6254f6a21f40c",
    "project": "5abf81b88c0d217615448d01",
    "type": "DRONE",
    "mission_data": "{\"name\":  \"mission1\"}",
    "name": "side mission",
    "description": "testing",
    "createdAt": "2019-02-20T10:28:34.348Z",
    "updatedAt": "2019-02-20T10:28:34.348Z",
    "__v": 0
}
 * 
 * 
 *  @apiErrorExample {json}  Server error
 *    HTTP/1.1 500 Internal Server Error
 *    {
 *       success: false,
 *       status: 'Error'
 *    }
 * @apiErrorExample {json} DB_Error
 *    HTTP/1.1 500 No entry in database
 *    {
 *       success: false,
 *       status: 'Error'
 *    }
*/

/**
*
* @api {patch} /api/mission-management/mission/:missionId  update the mission by id
* @apiName update_mission
* @apiGroup Missions
* @apiVersion  1.0.0
*
*
* @apiParam  {uuid} :id mission's id
*
* @apiExample Example usage:
*    http://52.66.177.173:8001/api/mission-management/mission/5c6d2bd26ed6254f6a21f40c
* 
* @apiParamExample  {Object} Request-Example:
* {
    "mission_data":{"name": "mission7"}
*}
* @apiSuccessExample Success-Response:
*
{
    "steps": [
        {
            "name": "FLIGHT"
        },
        {
            "name": "UPLOAD"
        },
        {
            "name": "IMAGE_PROCESS",
            "child_steps": [
                {
                    "name": "ODM_PROCESS"
                },
                {
                    "name": "PC_POST",
                    "child_steps": [
                        {
                            "name": "LASZIP"
                        },
                        {
                            "name": "SNIP_PC"
                        },
                        {
                            "name": "POTREE_CONV"
                        }
                    ]
                },
                {
                    "name": "ORTHO_POST",
                    "child_steps": [
                        {
                            "name": "SNIP_ORTHO"
                        },
                        {
                            "name": "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name": "CAMERAS"
                },
                {
                    "name": "THUMBNAILS"
                },
                {
                    "name": "UPLOAD_S3"
                }
            ]
        }
    ],
    "_id": "5c6aaf9463186b0ef9a549bd",
    "project": "5abf81b88c0d217615448d01",
    "type": "DRONE",
    "mission_data": "{\"name\": \"mission7\"}",
    "name": "side mission",
    "description": "testing",
    "createdAt": "2019-02-18T13:13:56.797Z",
    "updatedAt": "2019-02-20T10:30:43.372Z",
    "__v": 0
}

* @apiErrorExample {json}  Server error
*    HTTP/1.1 500 Internal Server Error
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} DB_Error
*    HTTP/1.1 500 No entry in database
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} MissionNotFound
*    HTTP/1.1 400 Invalid mission id given
*    {
*       success: false,
*       status: 'Error'
*    }
*/


/**
*
* @api {del} /api/mission-management/mission/:missionId delete the mission by id
* @apiName delete_mission
* @apiGroup Missions
* @apiVersion  1.0.0
*
*
* @apiParam  {uuid} :missionId mission's id  
* @apiSuccess (Success 200) {String} mission deleted mission response
*
* @apiParamExample  {Object} Request-Example:
* 
* http://52.66.177.173:8001/api/mission-management/mission/5c6d2bd26ed6254f6a21f40c
*
* @apiSuccessExample Success-Response:
*
{
    "steps": [
        {
            "name": "FLIGHT"
        },
        {
            "name": "UPLOAD"
        },
        {
            "name": "IMAGE_PROCESS",
            "child_steps": [
                {
                    "name": "ODM_PROCESS"
                },
                {
                    "name": "PC_POST",
                    "child_steps": [
                        {
                            "name": "LASZIP"
                        },
                        {
                            "name": "SNIP_PC"
                        },
                        {
                            "name": "POTREE_CONV"
                        }
                    ]
                },
                {
                    "name": "ORTHO_POST",
                    "child_steps": [
                        {
                            "name": "SNIP_ORTHO"
                        },
                        {
                            "name": "ORTHO_TILES"
                        }
                    ]
                },
                {
                    "name": "CAMERAS"
                },
                {
                    "name": "THUMBNAILS"
                },
                {
                    "name": "UPLOAD_S3"
                }
            ]
        }
    ],
    "_id": "5c6d2bd26ed6254f6a21f40c",
    "project": "5abf81b88c0d217615448d01",
    "type": "DRONE",
    "mission_data": "{\"name\":  \"mission1\"}",
    "name": "side mission",
    "description": "testing",
    "createdAt": "2019-02-20T10:28:34.348Z",
    "updatedAt": "2019-02-20T10:28:34.348Z",
    "__v": 0
}     
* @apiErrorExample {json}  Server error
*    HTTP/1.1 500 Internal Server Error
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} DB_Error
*    HTTP/1.1 500 No entry in database
*    {
*       success: false,
*       status: 'Error'
*    }
* @apiErrorExample {json} MissionNotFound
*    HTTP/1.1 400 Invalid mission id given
*    {
*       success: false,
*       status: 'Error'
*    }
*/


