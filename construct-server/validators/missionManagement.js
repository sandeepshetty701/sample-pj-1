var { check } = require('express-validator/check');

module.exports = {
    checkCreateMission : [
        check('type').exists().withMessage("mission type is required"),
        check('name').exists().withMessage("name for mission is required"),
        check('mission_data').exists().withMessage("mission_data is required")
    ],
    checkUpdateMission : [
        check('mission_data').exists().withMessage("mission_data is required")
    ]
}

