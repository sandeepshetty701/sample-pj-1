var { check } = require('express-validator/check');

module.exports = {
    checkCreateJob : [
        check('missionId').exists().withMessage("missionId: Mission Id for which this job is being created is required"),
        check('date').exists().withMessage("date: Date on which the data needs to be captured is required. yyyy-mm-dd hh:mm:ss format")
    ],
    checkUpdateJob : [
        check('stepName').exists().withMessage("stepName: Name of the step is required e.g: parent_step#child1#child2"),
        check('action').exists().withMessage("action: Action to be performed on the step is required. Only START or END supported."),
        check('stepData.time').exists().withMessage("stepData.time: Time at which the step action is taken is required. yyyy-mm-dd hh:mm:ss format")
    ]
}